# Golang-Demo

This repository contains a demo Golang application. The goal of this
application is to periodically monitor the current weather conditions of
some number of locations.

## Key Files

 * [OpenAPI Spec](./contrib/api/openapi.yaml)
 * [Example API Requests](./contrib/examples/)
 * [API Implementation](./internal/api/api.go)
   * [API Unit Tests](./internal/api/api_test.go)
 * [Data Source Schema](./internal/data_source/data_source_base/base.go)
   * [National Weather Service Implementation](./internal/data_source/data_source_nws/source.go)
     * [NWS Tests](./internal/data_source/data_source_nws/source_test.go)
 * [Location Monitor Controller](./internal/controller/location_monitor_controller/controller.go)
   * [Location Monitor Controller Unit Tests](./internal/controller/location_monitor_controller/controller_test.go)
 * [Location Monitor Created Controller](./internal/controller/location_monitor_created_controller/controller.go)
   * [Location Monitor Created Controller Unit Tests](./internal/controller/location_monitor_created_controller/controller_test.go)
 * [Database Implementation](./internal/store/locationMonitor.go)
   * [Database Tests](./internal/store/locationMonitor_test.go)

## Examples

```
$ ./contrib/examples/createMany.sh 
{"location_monitor":{"created_at":"2023-10-02T14:37:16Z","current_weather_description":"unknown","data_source":"nws/v1","latitude":36.85293,"longitude":-75.97799,"name":"Virginia Beach, Virginia","status":"active","updated_at":"2023-10-02T14:37:16Z","uuid":"0de488c3-bf27-45fd-8688-5aa4cf66d210"}}
...
$ curl http://localhost:8080/locations | jq .
{
  "location_monitors": [
    {
      "created_at": "2023-10-02T14:37:17Z",
      "current_max_temp": 30.555555555555557,
      "current_min_temp": 28.333333333333332,
      "current_weather_description": "rain",
      "data_source": "nws/v1",
      "latitude": 18.466333,
      "longitude": -66.105721,
      "name": "San Juan, Puerto Rico",
      "status": "active",
      "updated_at": "2023-10-02T14:37:54Z",
      "uuid": "1d5c5fae-d241-4cfc-9782-c5b15a06539a"
    },
...
$ curl http://localhost:8080/locations/1d5c5fae-d241-4cfc-9782-c5b15a06539a | jq .
{
  "location_monitor": {
    "created_at": "2023-10-02T14:37:17Z",
    "current_max_temp": 30.555555555555557,
    "current_min_temp": 28.333333333333332,
    "current_weather_description": "rain",
    "data_source": "nws/v1",
    "latitude": 18.466333,
    "longitude": -66.105721,
    "name": "San Juan, Puerto Rico",
    "status": "active",
    "updated_at": "2023-10-02T14:37:54Z",
    "uuid": "1d5c5fae-d241-4cfc-9782-c5b15a06539a"
  }
}
```

## Concepts

 * [Rest API](./internal/api/api.go) using [OpenAPI Spec](./contrib/api/openapi.yaml) and [generated golang code](./internal/api/api_gen/api.gen.go)
 * [Periodic Asyncronous ETL (extract transform load)](./internal/controller/location_monitor_controller/controller.go) with [multiple data sources](./internal/data_source/)
 * [Event based Asyncronous ETL](./internal/controller/location_monitor_created_controller/controller.go)
 * [PostgreSQL based persistence backend](./internal/store/locationMonitor.go)
 * Application wide tracing
 * [Docker-compose base development environment](./docker-compose.yaml)

## Caveats

This is certainly not a 100% perfect production grade implementation. It simply
exists to demonstrate proficency without diving too deep on any one rabbit hole.
As such there are many caveats that could be addressed.

 * Testing
    * I've attempted to provide basic happy-path and some edge case tests, but
    coverage could certainly be higher and more complex edge cases could be tested.
 * Documentation
    * I've attempted to document areas of the code that might be confusing, or that
    would be likely to be re-used (library code). Without peer-review, it can sometimes
    be hard to identify areas of code that might be confusing to someone coming in
    with fresh eyes. Documentation could likely be improved.
 * API and Object Design
    * Given this is an ETL application with multiple data-sources, it is very likely a
    future data-source may not perfectly fit the existing schema. As such, if I had more
    time, I'd want to build versioning into most objects used by the API and the data_source
    package.
 * Security
    * The REST API for this application has no concept of tenants or authX. This is
    obviously not a good thing for production.
 * Stability and Resilience
    * Given the reliance on external data-sources, retries and rate-limiting are a major
    concern. Due to time limitations I have omitted a retry and rate-limiting mechanism.
    In production, I would certainly want to wrap external client calls with such features.
    Likewise, having dedicated worker pools per data-source might make sense.
 * Metrics
    * I have not set up and custom OpenTelemetry's Metrics. In a production environment,
    I would certainly want some. Especially for async controller processes or large scale
    functionality that could overwhelm tracing.
 * Logging
    * With this application, I wanted to explore tracing rather than logging. In a production
    application, I might want to have both.
 * Database
    * Database migrations are a complex subject with many solutions. This is definitely
    not a production grade method for handling migrations. Something like Atlas or
    Alembic (in Python) would be a much better solution.
    * Only the most recent observation is stored, but in a ETL application, it might be a good
    idea to maintain historical observations. For this, I would likely suggest using
    something a bit more time-series friendly than Postgres. Perhaps Google BigTable.
 * Cloud Infra
    * Cloud Infra management is also a complex subject with many solutions. In production,
    I would want to use something like Terraform.

 ## Development

 The two main requirements for this project are Golang and Docker.

 This project uses a [Makefile](./Makefile) to lightly script common development tasks.

 ### Make Targets

  * `make deps`: Installs golang dependences
  * `make generate`: Generates any automatically generated OpenAPI or Mock code
  * `make test`: Runs golang unit tests
  * `make docker-image`: Builds the docker image
  * `make dev-up`: Starts or rebuilds the docker-compose environment
  * `make dev-down`: Tears down the docker-compose environment
  * `make e2e-test`: Runs E2E test suite against local docker-compose environment

### Docker Compose Endpoints

 * [API Endpoint](http://localhost:8080)
 * [Jaeger Tracing WebUI](http://localhost:16686)

 ## Design

 ![Application Design](./contrib/docs/design.png "Application Design")

### Objects

The primary object in this application is a "Location Monitor". This objects represents the
desire to periodically monitor the weather at a given point on Earth. At it's most basic,
a Location Monitor has a `name`, `data_source`, `longitude` and `latitude`.

```json
    {
        "name": "Virginia Beach, Virginia",
        "data_source": "nws/v1",
        "longitude": -75.97799,
        "latitude": 36.85293,
    }
```

Once populated, it will have a number of extra fields. Most notibly `current_weather_description`, `current_min_temp`, and `current_max_temp`. These fields are populated from the external Weather DataSource.

```json
    {
        "created_at": "2023-10-01T20:00:46Z",
        "updated_at": "2023-10-01T20:00:46Z",
        "uuid": "1b2db53e-9680-434d-b65a-5ebf96addea2",
        "status":"active",
        "name": "Virginia Beach, Virginia",
        "data_source": "nws/v1", 
        "longitude": -75.97799,
        "latitude": 36.85293,
        "current_weather_description":"clear",
        "current_min_temp": 17.22222222222222,
        "current_max_temp": 23.88888888888889
    }
```

### Components

  * Location Monitor Store: The PostgreSQL based database for persisting Location Monitors
  * API: The REST API of the application
    * Used by users to manage Location Monitors
    * Used by internal services to record Weather Observations for each Location Monitor
  * Location Monitor Controller: The controller responsible for periodically recording
    Weather Observations for each registered Location Monitor
  * Location Monitor Created Controller: The controller responsible for the initial
    Weather Observation for a newly registered Location Monitor

#### Controllers

The Application is split into two controllers. The first controller, the Location Monitor
Controller, is responsible for periodically crawling all registered Location Monitors. 
For each Location Monitor, it will check it's Weather DataSource for a new observation.
It will then record that Weather Observation in the database.

Because the Location Monitor Controller is run on a periodic interval, there may be a
period of time where a new Location Monitor might not have a current Weather Observation.
Rather than wait for the next sync by the Location Monitor Controller, another controller
is responsible for the first Observation for a new Location Monitor. When a new Location
Monitor is created, the API publishes an Event. The Location Monitor Created Controller
listens for this event, and it handles the first Observation.

## Tracing

### Listing Traces
![Example Traces](./contrib/docs/traces.png "Example Trace")

### Singular Trace Details
![Example Trace](./contrib/docs/trace.png "Example Trace")