//go:build e2e
// +build e2e

package e2e

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/default_http"
)

type E2ETestSuite struct {
	suite.Suite

	client api_gen.ClientInterface
}

func (s *E2ETestSuite) SetupTest() {
	var err error
	s.client, err = api_gen.NewClient("http://localhost:8080", api_gen.WithHTTPClient(default_http.NewClient()))
	s.Require().NoError(err)
}

func (s *E2ETestSuite) TestNoop() {
	ctx := context.Background()
	name := "Virginia Beach, Virginia"
	createReq := api_gen.CreateLocationMonitorRequestBody{
		LocationMonitor: api_gen.NewLocationMonitor{
			Name:       &name,
			DataSource: api_gen.NewLocationMonitorDataSourceNoopv1,
			Longitude:  -75.97799,
			Latitude:   36.85293,
		},
	}
	httpResp, err := s.client.CreateLocationMonitor(ctx, createReq)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	createResp, err := api_gen.ParseCreateLocationMonitorResponse(httpResp)
	s.Require().NoError(err)

	createdLM := createResp.JSON200.LocationMonitor

	httpResp, err = s.client.GetLocationMonitor(ctx, createdLM.Uuid)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	getResp, err := api_gen.ParseGetLocationMonitorResponse(httpResp)
	s.Require().NoError(err)

	lm := getResp.JSON200.LocationMonitor
	s.Require().Equal(api_gen.Unknown, lm.CurrentWeatherDescription)
	s.Require().Nil(lm.CurrentMinTemp)
	s.Require().Nil(lm.CurrentMaxTemp)

	recordReq := api_gen.RecordWeatherObservationRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.Clear,
			CurrentMinTemp:            63.0,
			CurrentMaxTemp:            74.0,
		},
	}
	httpResp, err = s.client.RecordWeatherObservation(ctx, createdLM.Uuid, recordReq)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	httpResp, err = s.client.GetLocationMonitor(ctx, createdLM.Uuid)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	getResp, err = api_gen.ParseGetLocationMonitorResponse(httpResp)
	s.Require().NoError(err)

	lm = getResp.JSON200.LocationMonitor
	s.Require().Equal(api_gen.Clear, lm.CurrentWeatherDescription)
	s.Require().NotNil(lm.CurrentMinTemp)
	s.Require().Equal(63.0, *lm.CurrentMinTemp)
	s.Require().NotNil(lm.CurrentMaxTemp)
	s.Require().Equal(74.0, *lm.CurrentMaxTemp)

	httpResp, err = s.client.DeleteLocationMonitor(ctx, createdLM.Uuid)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, httpResp.StatusCode)
}

func TestE2ETestSuite(t *testing.T) {
	suite.Run(t, new(E2ETestSuite))
}
