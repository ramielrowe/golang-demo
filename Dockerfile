FROM golang:1.21

WORKDIR /src

ADD go.mod /src/go.mod
ADD go.sum /src/go.sum
RUN go mod download

ADD cmd /src/cmd
ADD internal /src/internal
ADD tools.go /src/tools.go

RUN go build -o /bin/golang-demo-dev-init ./cmd/dev-init/main.go
RUN go build -o /bin/golang-demo-api ./cmd/api/main.go
RUN go build -o /bin/golang-demo-location-monitor-controller ./cmd/location-monitor-controller/main.go
RUN go build -o /bin/golang-demo-location-monitor-created-controller ./cmd/location-monitor-created-controller/main.go