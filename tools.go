//go:build tools
// +build tools

package main

import (
	_ "github.com/deepmap/oapi-codegen/cmd/oapi-codegen"
	_ "github.com/getkin/kin-openapi/cmd/validate"
	_ "go.uber.org/mock/mockgen"
)
