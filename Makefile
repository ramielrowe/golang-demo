SHELL=/bin/bash

OPENAPI_FILE=contrib/api/openapi.yaml
OPENAPI_GEN_FILE=internal/api/api_gen/api.gen.go

DOCKER_IMAGE=ramielrowe/golang-demo
DOCKER_TAG=$(shell git describe --always --dirty)

.PHONY: deps
deps:
	go mod download

.PHONY: test
test: deps
	go test -cover ./...

.PHONY: e2e-test
e2e-test: deps
	go test --tags=e2e ./test/e2e/...

.PHONY: generate
generate: generate-openapi generate-mocks

.PHONY: validate-openapi
validate-openapi:
	go run github.com/getkin/kin-openapi/cmd/validate -- $(OPENAPI_FILE)

.PHONY: generate-openapi
generate-openapi:
	go run github.com/deepmap/oapi-codegen/cmd/oapi-codegen \
		-package api_gen \
		-generate types,client,gorilla \
		$(OPENAPI_FILE) > $(OPENAPI_GEN_FILE)

.PHONY: generate-mocks
generate-mocks:
	go run go.uber.org/mock/mockgen \
		-source internal/store/locationMonitor.go \
		-package store_mocks \
		-destination internal/store/store_mocks/locationMonitor.go \
		LocationMonitorStore

	go run go.uber.org/mock/mockgen \
		-source internal/api/api_gen/api.gen.go \
		-package api_mocks \
		-destination internal/api/api_mocks/api.gen.go \
		ClientInterface

	go run go.uber.org/mock/mockgen \
		-source internal/data_source/data_source_base/base.go \
		-package data_source_mock \
		-destination internal/data_source/data_source_mock/mock.go \
		DataSource

.PHONY: docker-image
docker-image:
	docker build -t $(DOCKER_IMAGE):$(DOCKER_TAG) .

.PHONY: dev-up
dev-up:
	docker-compose up --build -d

	@echo Development stack running, services available:
	@echo API: http://localhost:8080
	@echo Jaeger: http://localhost:16686

.PHONY: dev-down
dev-down:
	docker-compose down --remove-orphans -v