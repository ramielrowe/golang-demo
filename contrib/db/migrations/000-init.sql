CREATE TABLE IF NOT EXISTS location_monitors (
    id bigserial PRIMARY KEY,
    uuid uuid UNIQUE NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL,
    name varchar(256) NOT NULL,
    status varchar(32) NOT NULL,
    data_source varchar(256) NOT NULL,
    longitude double precision NOT NULL,
    latitude double precision NOT NULL,
    current_weather_description TEXT NOT NULL,
    current_min_temp double precision,
    current_max_temp double precision
);

CREATE INDEX CONCURRENTLY IF NOT EXISTS 
    idx_lm_created_at_id ON location_monitors (created_at DESC, id DESC);