#!/bin/bash

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Virginia Beach, Virginia", "longitude": -75.97799, "latitude": 36.85293, "data_source": "noop/v1"}}' \
    http://localhost:8080/locations