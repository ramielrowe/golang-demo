#!/bin/bash

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Virginia Beach, Virginia", "longitude": -75.97799, "latitude": 36.85293, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Richmond, Virginia", "longitude": -77.434769, "latitude": 37.541290, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Charlottesville, Virginia", "longitude": -78.507980, "latitude": 38.033554, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Austin, Texas", "longitude": -97.733330, "latitude": 30.266666, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "San Francisco, California", "longitude": -122.431297, "latitude": 37.773972, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "New York, New York", "longitude": -73.935242, "latitude": 40.730610, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Chicago, Illinois", "longitude": -87.623177, "latitude": 41.881832, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Portland, Oregon", "longitude": -122.676483, "latitude": 45.523064, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Seattle, Washington", "longitude": -122.335167, "latitude": 47.608013, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "Key West, Florida", "longitude": -81.779984, "latitude": 24.555059, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations

curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"location_monitor": {"name": "San Juan, Puerto Rico", "longitude": -66.105721, "latitude": 18.466333, "data_source": "nws/v1"}}' \
    http://localhost:8080/locations