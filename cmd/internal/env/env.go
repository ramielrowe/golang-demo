package env

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"gitlab.com/ramielrowe/golang-demo/internal/server"
)

const (
	HTTPAddr         = "HTTP_ADDR"
	OtelGRPCEndpoint = "OTEL_GRPC_ENDPOINT"
	OtelGRPCInsecure = "OTEL_GRPC_INSECURE"
)

func ServerConfig(prefix string) server.ServerConfig {
	var config server.ServerConfig

	config.HTTPAddr = os.Getenv(prefix + HTTPAddr)
	if config.HTTPAddr == "" {
		config.HTTPAddr = "0.0.0.0:8080"
	}

	config.OtelGRPCEndpoint = os.Getenv(prefix + OtelGRPCEndpoint)

	otelGRPCInsecureStr := os.Getenv(prefix + OtelGRPCInsecure)
	if otelGRPCInsecureStr == "" {
		otelGRPCInsecureStr = "false"
	}
	otelGRPCInsecure, err := strconv.ParseBool(otelGRPCInsecureStr)
	if err != nil {
		panic(fmt.Sprintf("Invalid OtelGRPCInsecure value: %s", err.Error()))
	}
	config.OtelGRPCInsecure = otelGRPCInsecure
	return config
}

const (
	DBHost     = "DB_HOST"
	DBPort     = "DB_PORT"
	DBUSer     = "DB_USER"
	DBPassword = "DB_PASSWORD"
	DBDatabase = "DB_DATABASE"
	DBSSLMode  = "DB_SSLMODE"
)

type DBConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
	SSLMode  string
}

func (c DBConfig) ConnectionString() string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s password='%s' dbname=%s sslmode=%s",
		c.Host, c.Port, c.User, c.Password, c.Database, c.SSLMode)
}

const (
	StorePrefix = "STORE_"
)

func StoreDB(prefix string) DBConfig {
	var config DBConfig
	config.Host = os.Getenv(prefix + StorePrefix + DBHost)
	config.Port = os.Getenv(prefix + StorePrefix + DBPort)
	if config.Port == "" {
		config.Port = "5432"
	}
	config.User = os.Getenv(prefix + StorePrefix + DBUSer)
	config.Password = os.Getenv(prefix + StorePrefix + DBPassword)
	config.Database = os.Getenv(prefix + StorePrefix + DBDatabase)
	config.SSLMode = os.Getenv(prefix + StorePrefix + DBSSLMode)
	if config.SSLMode == "" {
		config.SSLMode = "disable"
	}
	return config
}

type ClientConfig struct {
	Server string
}

const (
	ClientPrefix = "CLIENT_"
)

func APIClient(prefix string) ClientConfig {
	var config ClientConfig

	config.Server = os.Getenv(prefix + ClientPrefix + "SERVER")

	return config
}

type LocationMonitorControllerConfig struct {
	RefreshInterval       time.Duration
	DataSourceWorkerCount int
}

const (
	RefreshInterval       = "REFRESH_INTERVAL"
	DataSourceWorkerCount = "DATASOURCE_WORKER_COUNT"
)

func LocationMonitorController(prefix string) LocationMonitorControllerConfig {
	var config LocationMonitorControllerConfig
	var err error

	refreshIntervalStr := os.Getenv(prefix + RefreshInterval)
	config.RefreshInterval, err = time.ParseDuration(refreshIntervalStr)
	if err != nil {
		panic(fmt.Sprintf("Invalid Refresh Interval value: %s", err.Error()))
	}

	DataSourceWorkerCountStr := os.Getenv(prefix + DataSourceWorkerCount)
	config.DataSourceWorkerCount, err = strconv.Atoi(DataSourceWorkerCountStr)
	if err != nil {
		panic(fmt.Sprintf("Invalid DataSource Worker Count value: %s", err.Error()))
	}

	return config
}

type EventHandlerConfig struct {
	LocationMonitorCreatedTopic string
}

const (
	LocationMonitorCreatedTopic = "LOCATION_MONITOR_CREATED_TOPIC"
)

func EventHandler(prefix string) EventHandlerConfig {
	return EventHandlerConfig{
		LocationMonitorCreatedTopic: os.Getenv(prefix + LocationMonitorCreatedTopic),
	}
}

type LocationMonitorCreatedControllerConfig struct {
	LocationMonitorCreatedSubscription string
}

const (
	LocationMonitorCreatedSub = "LOCATION_MONITOR_CREATED_SUB"
)

func LocationMonitorCreatedController(prefix string) LocationMonitorCreatedControllerConfig {
	return LocationMonitorCreatedControllerConfig{
		LocationMonitorCreatedSubscription: os.Getenv(prefix + LocationMonitorCreatedSub),
	}
}
