package dev

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/ramielrowe/golang-demo/internal/default_http"
)

func WaitForPubSubEmulator() {
	host := os.Getenv("PUBSUB_EMULATOR_HOST")
	if host == "" {
		return
	}
	for {
		resp, err := default_http.NewClient().Get("http://" + host)
		if err != nil {
			fmt.Println("error from pubsub check: " + err.Error())
		} else if resp.StatusCode != http.StatusOK {
			fmt.Println("status code from pubsub check: " + resp.Status)
		} else {
			fmt.Println("pubsub up")
			return
		}

		time.Sleep(time.Second)
	}
}

func WaitForDevInit() {
	host := os.Getenv("DEV_INIT_HOST")
	if host == "" {
		return
	}
	for {
		resp, err := default_http.NewClient().Get("http://" + host + "/healthz")
		if err != nil {
			fmt.Println("error from dev-init check: " + err.Error())
		} else if resp.StatusCode != http.StatusOK {
			fmt.Println("status code from dev-init check: " + resp.Status)
		} else {
			fmt.Println("dev-init up")
			return
		}

		time.Sleep(time.Second)
	}
}

func WaitForDev() {
	WaitForPubSubEmulator()
	WaitForDevInit()
}
