package main

import (
	"fmt"

	"gitlab.com/ramielrowe/golang-demo/cmd/internal/dev"
	"gitlab.com/ramielrowe/golang-demo/cmd/internal/env"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/controller/location_monitor_controller"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_nws"
	"gitlab.com/ramielrowe/golang-demo/internal/default_http"
	"gitlab.com/ramielrowe/golang-demo/internal/server"
	"go.opentelemetry.io/otel"
)

const (
	EnvPrefix       = "LOCATION_MONITOR_CONTROLLER_"
	OtelServiceName = "golang-demo-location-monitor-controller"
)

func main() {
	dev.WaitForDevInit()

	srvConfig := env.ServerConfig(EnvPrefix)
	srvConfig.OtelServiceName = OtelServiceName

	srv := server.NewServer(srvConfig)

	otel.SetTracerProvider(srv.TracerProvider)

	apiClientConfig := env.APIClient(EnvPrefix)

	apiClient, err := api_gen.NewClient(apiClientConfig.Server, api_gen.WithHTTPClient(default_http.NewClient()))
	if err != nil {
		panic(fmt.Sprintf("failed setting up store db: %s", err.Error()))
	}
	tracedClient := apiClient.WithTracer(srv.TracerProvider)

	dataSources := map[data_source_base.DataSourceName]data_source_base.DataSource{
		data_source_nws.DataSourceNameNWS_v1: data_source_nws.NewDefaultNWSSource(srv.TracerProvider),
	}

	ctrlEnvConfig := env.LocationMonitorController(EnvPrefix)
	ctrlConfig := location_monitor_controller.LocationMonitorControllerConfig{
		Tracer:                srv.TracerProvider.Tracer("LocationMonitorController"),
		Sources:               dataSources,
		Client:                tracedClient,
		RefreshInterval:       ctrlEnvConfig.RefreshInterval,
		DataSourceWorkerCount: ctrlEnvConfig.DataSourceWorkerCount,
	}

	ctrl := location_monitor_controller.NewLocationMonitorController(ctrlConfig)
	srv.RegisterController(ctrl)

	srv.Start()
	srv.Logger.Info("Started Server")
	srv.Wait()
	srv.Logger.Info("Server Stopped")
}
