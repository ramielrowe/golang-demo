package main

import (
	"context"
	"fmt"

	"cloud.google.com/go/pubsub"
	"github.com/uptrace/opentelemetry-go-extra/otelsql"
	"gitlab.com/ramielrowe/golang-demo/cmd/internal/dev"
	"gitlab.com/ramielrowe/golang-demo/cmd/internal/env"
	"gitlab.com/ramielrowe/golang-demo/internal/api"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_events"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/server"
	"gitlab.com/ramielrowe/golang-demo/internal/store"
	"go.opentelemetry.io/otel"
)

const (
	EnvPrefix       = "API_"
	OtelServiceName = "golang-demo-api"
)

func CreatePubSubEventHandler(srv *server.Server) *api_events.PubSubEventHandler {
	pubSubClient, err := pubsub.NewClient(context.Background(), "golang-demo-dev")
	if err != nil {
		panic(fmt.Sprintf("Failed to create PubSub Client: %s", err.Error()))
	}

	config := env.EventHandler(EnvPrefix)

	return api_events.NewPubSubEventHandler(srv.TracerProvider, pubSubClient.Topic(config.LocationMonitorCreatedTopic))
}

func main() {
	dev.WaitForDevInit()

	srvConfig := env.ServerConfig(EnvPrefix)
	srvConfig.OtelServiceName = OtelServiceName

	srv := server.NewServer(srvConfig)

	otel.SetTracerProvider(srv.TracerProvider)

	storeConfig := env.StoreDB(EnvPrefix)
	db, err := otelsql.Open(
		"postgres",
		storeConfig.ConnectionString(),
		otelsql.WithTracerProvider(srv.TracerProvider),
		otelsql.WithMeterProvider(srv.MeterProvider))

	if err != nil {
		panic(fmt.Sprintf("failed setting up store db: %s", err.Error()))
	}

	lmStore := store.NewLocationMonitorStore(db, srv.TracerProvider.Tracer("store"))

	apiServer := api.NewAPIServer(lmStore, CreatePubSubEventHandler(srv))
	api_gen.HandlerWithOptions(apiServer, api_gen.GorillaServerOptions{BaseRouter: srv.MuxRouter})

	srv.Start()
	srv.Logger.Info("Started Server")
	srv.Wait()
	srv.Logger.Info("Server Stopped")
}
