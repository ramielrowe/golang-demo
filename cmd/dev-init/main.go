package main

import (
	"context"
	"fmt"
	"net/http"

	"cloud.google.com/go/pubsub"
	"gitlab.com/ramielrowe/golang-demo/cmd/internal/dev"
	"gitlab.com/ramielrowe/golang-demo/cmd/internal/env"
	"gitlab.com/ramielrowe/golang-demo/internal/server"
)

const (
	EnvPrefix = "DEV_INIT_"
)

func createEventResources(ctx context.Context, client *pubsub.Client) {
	eventHandlerConfig := env.EventHandler(EnvPrefix)

	topic := client.Topic(eventHandlerConfig.LocationMonitorCreatedTopic)
	ok, err := topic.Exists(ctx)
	if !ok {
		if err != nil {
			panic(fmt.Sprintf("Failed to check LocationMonitorCreated Topic exists: %s", err.Error()))
		}

		topic, err = client.CreateTopic(ctx, eventHandlerConfig.LocationMonitorCreatedTopic)
		if err != nil {
			panic(fmt.Sprintf("Failed to create LocationMonitorCreated Topic: %s", err.Error()))
		}
	}

	ctrlConfig := env.LocationMonitorCreatedController(EnvPrefix)

	ok, err = client.Subscription(ctrlConfig.LocationMonitorCreatedSubscription).Exists(ctx)
	if !ok {
		if err != nil {
			panic(fmt.Sprintf("Failed to check LocationMonitorCreated Sub exists: %s", err.Error()))
		}

		_, err = client.CreateSubscription(ctx, ctrlConfig.LocationMonitorCreatedSubscription, pubsub.SubscriptionConfig{Topic: topic})
		if err != nil {
			panic(fmt.Sprintf("Failed to create LocationMonitorCreated Sub: %s", err.Error()))
		}
	}
}

func main() {
	ctx := context.Background()

	dev.WaitForPubSubEmulator()

	pubSubClient, err := pubsub.NewClient(ctx, "golang-demo-dev")
	if err != nil {
		panic(fmt.Sprintf("Failed to create PubSub Client: %s", err.Error()))
	}

	createEventResources(ctx, pubSubClient)

	srv := server.NewServer(env.ServerConfig(EnvPrefix))
	srv.MuxRouter.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })
	srv.Start()
	srv.Wait()
}
