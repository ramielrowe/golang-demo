package store

import (
	"errors"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq" // load Postgres driver

	sq "github.com/Masterminds/squirrel"
)

// Set query argument format for Postgres
var psq = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

var (
	ErrRecordNotFound           = errors.New("record not found")
	ErrFailedToInsertRow        = errors.New("failed to insert row")
	ErrMalformedPaginationToken = errors.New("malformed pagination token")
	ErrFailedToDeleteRow        = errors.New("failed to delete row")
)

const (
	PageLimit    = 100
	PageTokenSep = ";"
)

// parsePageToken returns the query predicate for the provided pagination token
func parsePageToken(token string) (sq.Or, error) {
	tokens := strings.Split(token, PageTokenSep)
	if len(tokens) != 2 {
		return sq.Or{}, ErrMalformedPaginationToken
	}
	timestamp, err := time.Parse(time.RFC3339Nano, tokens[0])
	if err != nil {
		return sq.Or{}, err
	}
	id, err := strconv.ParseInt(tokens[1], 10, 64)
	if err != nil {
		return sq.Or{}, err
	}
	return sq.Or{
		sq.And{sq.Eq{"created_at": timestamp}, sq.LtOrEq{"id": id}},
		sq.Lt{"created_at": timestamp},
	}, nil
}

// buildPageToken returns a string formatted pagination token for the given record
func buildPageToken(createdAt time.Time, id int64) string {
	return createdAt.Format(time.RFC3339Nano) + PageTokenSep + strconv.FormatInt(id, 10)
}
