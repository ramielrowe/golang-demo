package store

import (
	"encoding/hex"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func randomHex(n int) string {
	bytes := make([]byte, n)
	if _, err := random.Read(bytes); err != nil {
		panic(err)
	}
	return hex.EncodeToString(bytes)
}

const (
	testPageTokenTimestampString string = "2023-09-25T20:49:37.256796-04:00"
	testPageTokenID              int64  = 1245
	testPageToken                string = "2023-09-25T20:49:37.256796-04:00;1245"
)

func TestParsePageToken(t *testing.T) {
	pred, err := parsePageToken(testPageToken)
	require.NoError(t, err)

	qry, args, err := pred.ToSql()
	require.NoError(t, err)

	require.Equal(t, "((created_at = ? AND id <= ?) OR created_at < ?)", qry)
	require.Len(t, args, 3)

	expectedTimestamp, err := time.Parse(time.RFC3339, testPageTokenTimestampString)
	require.NoError(t, err)
	require.Equal(t, expectedTimestamp, args[0])
	require.Equal(t, testPageTokenID, args[1])
	require.Equal(t, expectedTimestamp, args[2])
}

func TestBuildPageToken(t *testing.T) {
	expectedTimestamp, err := time.Parse(time.RFC3339, testPageTokenTimestampString)
	require.NoError(t, err)
	token := buildPageToken(expectedTimestamp, testPageTokenID)
	require.Equal(t, testPageToken, token)
}
