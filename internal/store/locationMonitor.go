package store

import (
	"context"
	"database/sql"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"gitlab.com/ramielrowe/golang-demo/internal/common_trace"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type LocationMonitorStatus string

const (
	LocationMonitorTable = "location_monitors"

	LocationMonitorStatusActive = LocationMonitorStatus("active")
)

type LocationMonitor struct {
	id                        int64
	UUID                      uuid.UUID
	CreatedAt                 time.Time
	UpdatedAt                 time.Time
	Name                      string
	DataSource                data_source_base.DataSourceName
	Status                    LocationMonitorStatus
	Longitude                 float64
	Latitude                  float64
	CurrentWeatherDescription data_source_base.WeatherDescription
	CurrentMinTemp            *float64
	CurrentMaxTemp            *float64
}

type locationMonitorScanner struct{}

func (s locationMonitorScanner) Columns() []string {
	return []string{
		"id", "uuid", "created_at", "updated_at", "name",
		"data_source", "longitude", "latitude", "status",
		"current_weather_description", "current_min_temp",
		"current_max_temp",
	}
}

func (s locationMonitorScanner) ScanInto(row sq.RowScanner, l *LocationMonitor) error {
	return row.Scan(
		&l.id, &l.UUID, &l.CreatedAt, &l.UpdatedAt, &l.Name,
		&l.DataSource, &l.Longitude, &l.Latitude, &l.Status,
		&l.CurrentWeatherDescription, &l.CurrentMinTemp,
		&l.CurrentMaxTemp,
	)
}

type NewLocationMonitor struct {
	UUID       uuid.UUID
	Name       string
	DataSource data_source_base.DataSourceName
	Longitude  float64
	Latitude   float64
}

func (m NewLocationMonitor) setMap() map[string]interface{} {
	return map[string]interface{}{
		"uuid":                        m.UUID,
		"created_at":                  sq.Expr("NOW()"),
		"updated_at":                  sq.Expr("NOW()"),
		"name":                        m.Name,
		"data_source":                 m.DataSource,
		"longitude":                   m.Longitude,
		"latitude":                    m.Latitude,
		"status":                      LocationMonitorStatusActive,
		"current_weather_description": data_source_base.WeatherUnknown,
	}
}

type ListLocationMonitorsPage struct {
	LocationMonitors []*LocationMonitor
	PaginationToken  *string
}

type ListLocationMonitorsOptions struct {
	PaginationToken *string
}

type CreateLocationMonitorOptions struct {
	Callback func(ctx context.Context, id uuid.UUID) error
}

type LocationMonitorStore interface {
	CreateLocationMonitor(ctx context.Context, newMonitor NewLocationMonitor, opt CreateLocationMonitorOptions) error
	GetLocationMonitor(ctx context.Context, id uuid.UUID) (*LocationMonitor, error)
	ListLocationMonitors(ctx context.Context, opts ListLocationMonitorsOptions) (*ListLocationMonitorsPage, error)
	RecordWeatherObservation(ctx context.Context, locationID uuid.UUID, observation data_source_base.WeatherObservation) error
	DeleteLocationMonitor(ctx context.Context, id uuid.UUID) error
}

type locationMonitorStore struct {
	db     *sql.DB
	tracer trace.Tracer
}

var _ LocationMonitorStore = (*locationMonitorStore)(nil)

func NewLocationMonitorStore(db *sql.DB, tracer trace.Tracer) *locationMonitorStore {
	return &locationMonitorStore{db: db, tracer: tracer}
}

func (s *locationMonitorStore) CreateLocationMonitor(ctx context.Context, newMonitor NewLocationMonitor, opt CreateLocationMonitorOptions) error {
	ctx, span := s.tracer.Start(ctx, "locationMonitorStore.CreateLocationMonitor")
	defer span.End()

	span.SetAttributes(common_trace.LocationMonitorUUID.String(newMonitor.UUID.String()))

	qry := psq.Insert(LocationMonitorTable).SetMap(newMonitor.setMap()).Suffix("RETURNING id")
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		tx.Rollback()
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed starting transaction")
		return err
	}
	row := qry.RunWith(tx).QueryRowContext(ctx)

	var id int64
	err = row.Scan(&id)
	if err != nil {
		tx.Rollback()
		if err == sql.ErrNoRows {
			span.RecordError(ErrFailedToInsertRow)
			span.SetStatus(codes.Error, "failed to insert row")
			return ErrFailedToInsertRow
		}
		span.RecordError(err)
		return err
	}

	if id == 0 {
		tx.Rollback()
		span.RecordError(ErrFailedToInsertRow)
		span.SetStatus(codes.Error, "failed to insert row")
		return ErrFailedToInsertRow
	}

	if opt.Callback != nil {
		err = opt.Callback(ctx, newMonitor.UUID)
		if err != nil {
			tx.Rollback()
			span.RecordError(err)
			span.SetStatus(codes.Error, "failed calling callback")
			return err
		}
	}

	return tx.Commit()
}

func (s *locationMonitorStore) GetLocationMonitor(ctx context.Context, id uuid.UUID) (*LocationMonitor, error) {
	ctx, span := s.tracer.Start(ctx, "locationMonitorStore.GetLocationMonitor")
	defer span.End()

	span.SetAttributes(common_trace.LocationMonitorUUID.String(id.String()))

	var scanner locationMonitorScanner
	var lm LocationMonitor
	qry := psq.RunWith(s.db).Select(scanner.Columns()...).From(LocationMonitorTable).Where(sq.Eq{"uuid": id})
	err := scanner.ScanInto(qry.QueryRowContext(ctx), &lm)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrRecordNotFound
		}
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed scanning result")
		return nil, err
	}
	return &lm, nil
}

func (s *locationMonitorStore) ListLocationMonitors(ctx context.Context, opts ListLocationMonitorsOptions) (*ListLocationMonitorsPage, error) {
	ctx, span := s.tracer.Start(ctx, "locationMonitorStore.ListLocationMonitors")
	defer span.End()
	var scanner locationMonitorScanner
	qry := psq.RunWith(s.db).
		Select(scanner.Columns()...).
		From(LocationMonitorTable)

	if opts.PaginationToken != nil {
		span.SetAttributes(attribute.String("page_token", *opts.PaginationToken))
		pageCond, err := parsePageToken(*opts.PaginationToken)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, "failed parsing page token")
			return nil, err
		}
		qry = qry.Where(pageCond)
	}

	qry = qry.OrderBy("created_at DESC", "id DESC").
		Limit(PageLimit + 1) // One extra to build next pagination token

	rows, err := qry.QueryContext(ctx)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed running query")
		return nil, err
	}

	locationMonitors := make([]*LocationMonitor, PageLimit)
	var nextMonitor *LocationMonitor
	i := 0
	for rows.Next() {
		if i == PageLimit {
			// Scan next monitor to build next pagination token
			nextMonitor = &LocationMonitor{}
			err = scanner.ScanInto(rows, nextMonitor)
			if err != nil {
				span.RecordError(err)
				span.SetStatus(codes.Error, "failed scanning result")
				return nil, err
			}
			break
		}
		locationMonitors[i] = &LocationMonitor{}
		err = scanner.ScanInto(rows, locationMonitors[i])
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, "failed scanning result")
			return nil, err
		}
		i++
	}

	resp := ListLocationMonitorsPage{
		LocationMonitors: locationMonitors[:i],
	}

	if nextMonitor != nil {
		paginationToken := buildPageToken(nextMonitor.CreatedAt, nextMonitor.id)
		resp.PaginationToken = &paginationToken
	}

	return &resp, nil
}

type recordWeatherObservationUpdate struct {
	CurrentWeatherDescription data_source_base.WeatherDescription
	CurrentMinTemp            float64
	CurrentMaxTemp            float64
}

func (u *recordWeatherObservationUpdate) setMap() map[string]interface{} {
	return map[string]interface{}{
		"updated_at":                  sq.Expr("NOW()"),
		"current_weather_description": u.CurrentWeatherDescription,
		"current_min_temp":            u.CurrentMinTemp,
		"current_max_temp":            u.CurrentMaxTemp,
	}
}

func (s *locationMonitorStore) RecordWeatherObservation(ctx context.Context, locationID uuid.UUID, observation data_source_base.WeatherObservation) error {
	ctx, span := s.tracer.Start(ctx, "locationMonitorStore.RecordWeatherObservation")
	defer span.End()

	span.SetAttributes(common_trace.LocationMonitorUUID.String(locationID.String()))

	update := recordWeatherObservationUpdate{
		CurrentWeatherDescription: observation.CurrentDescription,
		CurrentMinTemp:            observation.CurrentMinTemp,
		CurrentMaxTemp:            observation.CurrentMaxTemp,
	}
	qry := psq.Update(LocationMonitorTable).
		SetMap(update.setMap()).
		Where(sq.Eq{"uuid": locationID}).
		Suffix("RETURNING id")

	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		tx.Rollback()
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed starting transaction")
		return err
	}

	row := qry.RunWith(tx).QueryRowContext(ctx)

	var id int64
	err = row.Scan(&id)
	if err != nil {
		tx.Rollback()
		if err == sql.ErrNoRows {
			return ErrRecordNotFound
		}
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed scanning result")
		return err
	}

	if id == 0 {
		tx.Rollback()
		span.RecordError(ErrRecordNotFound)
		span.SetStatus(codes.Error, "failed updating row")
		return ErrRecordNotFound
	}

	return tx.Commit()
}

func (s *locationMonitorStore) DeleteLocationMonitor(ctx context.Context, id uuid.UUID) error {
	ctx, span := s.tracer.Start(ctx, "locationMonitorStore.DeleteLocationMonitor")
	defer span.End()

	span.SetAttributes(common_trace.LocationMonitorUUID.String(id.String()))

	qry := psq.Delete(LocationMonitorTable).Where(sq.Eq{"uuid": id})
	result, err := qry.RunWith(s.db).ExecContext(ctx)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed deleting row")
		return err
	}
	i, err := result.RowsAffected()
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed deleting row")
		return err
	}
	if i == 0 {
		span.RecordError(ErrFailedToDeleteRow)
		span.SetStatus(codes.Error, "failed deleting row")
		return ErrFailedToDeleteRow
	}
	return nil
}
