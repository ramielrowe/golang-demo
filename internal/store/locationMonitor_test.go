package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_nws"
	"go.opentelemetry.io/otel"
)

type LocationMonitorStoreTestSuite struct {
	suite.Suite
	ctx context.Context

	dockerPool *dockertest.Pool
	dbResource *dockertest.Resource

	dbUser     string
	dbPassword string
	dbDatabase string

	testDatabase string

	db     *sql.DB
	TestDB *sql.DB

	store *locationMonitorStore
}

func (s *LocationMonitorStoreTestSuite) SetupSuite() {
	s.ctx = context.Background()

	dockerPool, err := dockertest.NewPool("")
	s.Require().NoError(err)

	s.dockerPool = dockerPool
	s.Require().NoError(s.dockerPool.Client.Ping())

	s.dbUser = randomHex(16)
	s.dbPassword = randomHex(16)
	s.dbDatabase = "testdb_" + randomHex(16)
	dbEnv := []string{
		fmt.Sprintf("POSTGRES_USER=%s", s.dbUser),
		fmt.Sprintf("POSTGRES_PASSWORD=%s", s.dbPassword),
		fmt.Sprintf("POSTGRES_DB=%s", s.dbDatabase),
	}

	s.dbResource, err = s.dockerPool.RunWithOptions(&dockertest.RunOptions{
		Repository: "library/postgres",
		Tag:        "16",
		Env:        dbEnv,
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})
	s.Require().NoError(err)

	s.T().Cleanup(func() { s.dockerPool.Purge(s.dbResource) })

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	s.Require().NoError(
		s.dockerPool.Retry(
			func() error {
				connString := fmt.Sprintf(
					"host=localhost port=%s user=%s password='%s' dbname=%s sslmode=disable",
					s.dbResource.GetPort("5432/tcp"), s.dbUser, s.dbPassword, s.dbDatabase)
				db, err := sql.Open("postgres", connString)
				if err != nil {
					return err
				}
				s.db = db
				err = db.Ping()
				return err
			}),
	)
}

func (s *LocationMonitorStoreTestSuite) SetupTest() {
	s.testDatabase = "testdb_" + randomHex(16)
	r, err := s.db.Query(fmt.Sprintf(`CREATE DATABASE "%s";`, s.testDatabase))
	s.Require().NoError(err)
	s.Require().NoError(r.Close())
	s.T().Cleanup(func() {
		s.db.Query(fmt.Sprintf(`DROP DATABASE "%s";`, s.testDatabase))
	})
	connString := fmt.Sprintf(
		"host=localhost port=%s user=%s password='%s' dbname=%s sslmode=disable",
		s.dbResource.GetPort("5432/tcp"), s.dbUser, s.dbPassword, s.testDatabase)
	db, err := sql.Open("postgres", connString)
	s.Require().NoError(err)
	s.TestDB = db
	err = db.Ping()
	s.Require().NoError(err)

	s.Require().NoError(migrationCreateLocationMonitorTable(s.ctx, s.TestDB))

	s.store = &locationMonitorStore{db: s.TestDB, tracer: otel.GetTracerProvider().Tracer("store")}
}

func (s *LocationMonitorStoreTestSuite) TestLocationMonitorHappyPath() {
	newLoc := NewLocationMonitor{
		UUID:       uuid.New(),
		Name:       "Virginia Beach, Virginia",
		DataSource: data_source_nws.DataSourceNameNWS_v1,
		Longitude:  -75.97799,
		Latitude:   36.85293,
	}

	var calledUUID uuid.UUID
	createCallback := func(ctx context.Context, id uuid.UUID) error {
		calledUUID = id
		return nil
	}

	s.Require().NoError(s.store.CreateLocationMonitor(s.ctx, newLoc, CreateLocationMonitorOptions{Callback: createCallback}))

	loc, err := s.store.GetLocationMonitor(s.ctx, newLoc.UUID)
	s.Require().NoError(err)
	s.Require().Equal(calledUUID.String(), loc.UUID.String())
	s.Require().Equal(newLoc.UUID.String(), loc.UUID.String())
	s.Require().False(loc.CreatedAt.IsZero())
	s.Require().False(loc.UpdatedAt.IsZero())
	s.Require().Equal(newLoc.Name, loc.Name)
	s.Require().Equal(newLoc.DataSource, loc.DataSource)
	s.Require().Equal(newLoc.Longitude, loc.Longitude)
	s.Require().Equal(newLoc.Latitude, loc.Latitude)
	s.Require().Equal(LocationMonitorStatusActive, loc.Status)
	s.Require().Equal(data_source_base.WeatherUnknown, loc.CurrentWeatherDescription)
	s.Require().Nil(loc.CurrentMinTemp)
	s.Require().Nil(loc.CurrentMaxTemp)

	page, err := s.store.ListLocationMonitors(s.ctx, ListLocationMonitorsOptions{})
	s.Require().NoError(err)
	s.Require().Len(page.LocationMonitors, 1)
	s.Require().Nil(page.PaginationToken)

	listedLoc := page.LocationMonitors[0]
	s.Require().Equal(newLoc.UUID.String(), listedLoc.UUID.String())
	s.Require().False(listedLoc.CreatedAt.IsZero())
	s.Require().False(listedLoc.UpdatedAt.IsZero())
	s.Require().Equal(newLoc.Name, listedLoc.Name)
	s.Require().Equal(newLoc.DataSource, listedLoc.DataSource)
	s.Require().Equal(newLoc.Longitude, loc.Longitude)
	s.Require().Equal(newLoc.Latitude, loc.Latitude)
	s.Require().Equal(LocationMonitorStatusActive, listedLoc.Status)
	s.Require().Equal(data_source_base.WeatherUnknown, listedLoc.CurrentWeatherDescription)
	s.Require().Nil(loc.CurrentMinTemp)
	s.Require().Nil(loc.CurrentMaxTemp)

	s.Require().NoError(s.store.DeleteLocationMonitor(s.ctx, newLoc.UUID))

	_, err = s.store.GetLocationMonitor(s.ctx, newLoc.UUID)
	s.Require().ErrorIs(err, ErrRecordNotFound)
}

func (s *LocationMonitorStoreTestSuite) TestRecordWeatherObservation() {
	newLoc := NewLocationMonitor{
		UUID:       uuid.New(),
		Name:       "Virginia Beach, Virginia",
		DataSource: data_source_nws.DataSourceNameNWS_v1,
		Longitude:  -75.97799,
		Latitude:   36.85293,
	}

	observation := data_source_base.WeatherObservation{
		CurrentDescription: data_source_base.WeatherClear,
		CurrentMinTemp:     63.0,
		CurrentMaxTemp:     74.0,
	}

	s.Require().NoError(s.store.CreateLocationMonitor(s.ctx, newLoc, CreateLocationMonitorOptions{}))

	loc, err := s.store.GetLocationMonitor(s.ctx, newLoc.UUID)
	s.Require().NoError(err)
	s.Require().Equal(newLoc.UUID.String(), loc.UUID.String())
	s.Require().False(loc.CreatedAt.IsZero())
	s.Require().False(loc.UpdatedAt.IsZero())
	s.Require().Equal(newLoc.Name, loc.Name)
	s.Require().Equal(newLoc.DataSource, loc.DataSource)
	s.Require().Equal(newLoc.Longitude, loc.Longitude)
	s.Require().Equal(newLoc.Latitude, loc.Latitude)
	s.Require().Equal(LocationMonitorStatusActive, loc.Status)
	s.Require().Equal(data_source_base.WeatherUnknown, loc.CurrentWeatherDescription)
	s.Require().Nil(loc.CurrentMinTemp)
	s.Require().Nil(loc.CurrentMaxTemp)

	s.Require().NoError(s.store.RecordWeatherObservation(s.ctx, newLoc.UUID, observation))

	loc, err = s.store.GetLocationMonitor(s.ctx, newLoc.UUID)
	s.Require().NoError(err)
	s.Require().Equal(newLoc.UUID.String(), loc.UUID.String())
	s.Require().False(loc.CreatedAt.IsZero())
	s.Require().False(loc.UpdatedAt.IsZero())
	s.Require().Equal(newLoc.Name, loc.Name)
	s.Require().Equal(newLoc.DataSource, loc.DataSource)
	s.Require().Equal(newLoc.Longitude, loc.Longitude)
	s.Require().Equal(newLoc.Latitude, loc.Latitude)
	s.Require().Equal(LocationMonitorStatusActive, loc.Status)
	s.Require().Equal(data_source_base.WeatherClear, loc.CurrentWeatherDescription)
	s.Require().NotNil(loc.CurrentMinTemp)
	s.Require().Equal(observation.CurrentMinTemp, *loc.CurrentMinTemp)
	s.Require().NotNil(loc.CurrentMaxTemp)
	s.Require().Equal(observation.CurrentMaxTemp, *loc.CurrentMaxTemp)
}

func (s *LocationMonitorStoreTestSuite) TestRecordWeatherObservationNotFound() {
	observation := data_source_base.WeatherObservation{
		CurrentDescription: data_source_base.WeatherClear,
		CurrentMinTemp:     63.0,
		CurrentMaxTemp:     74.0,
	}

	err := s.store.RecordWeatherObservation(s.ctx, uuid.New(), observation)
	s.Require().ErrorIs(err, ErrRecordNotFound)
}

func (s *LocationMonitorStoreTestSuite) TestCreateLocationMonitorCallbackErrorDoesntInsert() {
	newLoc := NewLocationMonitor{
		UUID:       uuid.New(),
		Name:       "Virginia Beach, Virginia",
		DataSource: data_source_nws.DataSourceNameNWS_v1,
		Longitude:  -75.97799,
		Latitude:   36.85293,
	}

	callbackErr := errors.New("failed")
	createCallback := func(ctx context.Context, id uuid.UUID) error {
		return callbackErr
	}

	err := s.store.CreateLocationMonitor(s.ctx, newLoc, CreateLocationMonitorOptions{Callback: createCallback})
	s.Require().ErrorIs(err, callbackErr)

	_, err = s.store.GetLocationMonitor(s.ctx, newLoc.UUID)
	s.Require().ErrorIs(err, ErrRecordNotFound)
}

func (s *LocationMonitorStoreTestSuite) TestListLocationMonitorsPagination() {
	for i := 0; i < 150; i++ {
		newLoc := NewLocationMonitor{
			UUID:       uuid.New(),
			Name:       "Virginia Beach, Virginia",
			DataSource: data_source_nws.DataSourceNameNWS_v1,
			Longitude:  -75.97799,
			Latitude:   36.85293,
		}
		s.Require().NoError(s.store.CreateLocationMonitor(s.ctx, newLoc, CreateLocationMonitorOptions{}))
	}

	page, err := s.store.ListLocationMonitors(s.ctx, ListLocationMonitorsOptions{})
	s.Require().NoError(err)
	s.Require().Len(page.LocationMonitors, 100)
	s.Require().NotNil(page.PaginationToken)

	foundMonitors := make(map[string]struct{})
	for _, monitor := range page.LocationMonitors {
		foundMonitors[monitor.UUID.String()] = struct{}{}
	}

	// Assert we found 100 unique LocationMonitors
	s.Require().Len(foundMonitors, 100)

	page, err = s.store.ListLocationMonitors(s.ctx, ListLocationMonitorsOptions{PaginationToken: page.PaginationToken})
	s.Require().NoError(err)
	s.Require().Len(page.LocationMonitors, 50)
	s.Require().Nil(page.PaginationToken)

	for _, monitor := range page.LocationMonitors {
		foundMonitors[monitor.UUID.String()] = struct{}{}
	}

	// Assert we found 50 more unique LocationMonitors
	s.Require().Len(foundMonitors, 150)
}

func TestLocationMonitorStoreTestSuite(t *testing.T) {
	suite.Run(t, new(LocationMonitorStoreTestSuite))
}
