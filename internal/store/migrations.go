package store

import (
	"context"
	"database/sql"
	"fmt"
)

// Migration takes a DB and applies a number of mutations to the database schema
type Migration func(ctx context.Context, db *sql.DB) error

func migrationCreateLocationMonitorTable(ctx context.Context, db *sql.DB) error {
	qry := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS %s (
		id bigserial PRIMARY KEY,
		uuid uuid UNIQUE NOT NULL,
		created_at timestamp NOT NULL,
		updated_at timestamp NOT NULL,
		name varchar(256) NOT NULL,
		status varchar(32) NOT NULL,
		data_source varchar(256) NOT NULL,
		longitude double precision NOT NULL,
		latitude double precision NOT NULL,
		current_weather_description TEXT NOT NULL,
		current_min_temp double precision,
		current_max_temp double precision
	);`, LocationMonitorTable)
	rows, err := db.QueryContext(ctx, qry)
	if err != nil {
		return err
	}
	err = rows.Close()
	if err != nil {
		return err
	}

	qry = fmt.Sprintf(`
		CREATE INDEX CONCURRENTLY IF NOT EXISTS idx_lm_created_at_id ON %s (created_at DESC, id DESC);
	`, LocationMonitorTable)
	rows, err = db.QueryContext(ctx, qry)
	if err != nil {
		return err
	}
	err = rows.Close()
	if err != nil {
		return err
	}

	return nil
}
