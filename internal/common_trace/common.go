package common_trace

import (
	"fmt"
	"net/http"

	"cloud.google.com/go/pubsub"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	LocationMonitorUUID = attribute.Key("location_monitor_uuid")
	DataSourceName      = attribute.Key("data_source_name")
)

func OTELMuxSpanName(routeName string, r *http.Request) string {
	return fmt.Sprintf("%s %s", r.Method, routeName)
}

const (
	MessageID     = attribute.Key("message_id")
	MessageAction = attribute.Key("message_action")
)

func SetAttrForMsg(span trace.Span, msg *pubsub.Message) {
	span.SetAttributes(MessageID.String(msg.ID))
}

func AckMsg(span trace.Span, msg *pubsub.Message) {
	span.SetAttributes(MessageAction.String("ack"))
	msg.Ack()
}

func NackMsg(span trace.Span, msg *pubsub.Message) {
	span.SetAttributes(MessageAction.String("nack"))
	msg.Nack()
}
