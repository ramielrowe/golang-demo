package iso8601_interval

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestJSONTimeTime(t *testing.T) {
	s := `"2007-03-01T13:00:00Z/2008-05-11T15:30:00Z"`
	var i ISO8601Interval
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(time.Date(2007, 03, 01, 13, 0, 0, 0, time.UTC)))
	require.True(t, i.End.Equal(time.Date(2008, 05, 11, 15, 30, 0, 0, time.UTC)))
}

func TestJSONNowNow(t *testing.T) {
	s := `"NOW/NOW"`
	var i ISO8601Interval
	expectedNow := time.Date(2023, 9, 19, 12, 14, 15, 0, time.UTC)
	i.timeNow = func() time.Time { return expectedNow }
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(expectedNow))
	require.True(t, i.End.Equal(expectedNow))
}

func TestJSONTimeDuration(t *testing.T) {
	s := `"2007-03-01T13:00:00Z/P1Y2M10DT2H30M"`
	var i ISO8601Interval
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(time.Date(2007, 03, 01, 13, 0, 0, 0, time.UTC)))
	require.True(t, i.End.Equal(time.Date(2008, 05, 10, 11, 30, 0, 0, time.UTC)))
}

func TestJSONNowDuration(t *testing.T) {
	s := `"NOW/P1Y"`
	var i ISO8601Interval
	expectedNow := time.Date(2023, 9, 19, 12, 14, 15, 0, time.UTC)
	i.timeNow = func() time.Time { return expectedNow }
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(expectedNow))
	require.True(t, i.End.Equal(expectedNow.Add(365*24*time.Hour)))
}

func TestJSONDurationTime(t *testing.T) {
	s := `"P1Y2M10DT2H30M/2008-05-11T15:30:00Z"`
	var i ISO8601Interval
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(time.Date(2007, 03, 02, 17, 0, 0, 0, time.UTC)))
	require.True(t, i.End.Equal(time.Date(2008, 05, 11, 15, 30, 0, 0, time.UTC)))
}

func TestJSONDurationNow(t *testing.T) {
	s := `"P1Y/NOW"`
	var i ISO8601Interval
	expectedNow := time.Date(2023, 9, 19, 12, 14, 15, 0, time.UTC)
	i.timeNow = func() time.Time { return expectedNow }
	err := json.Unmarshal([]byte(s), &i)
	require.NoError(t, err)
	require.True(t, i.Start.Equal(expectedNow.Add(-365*24*time.Hour)))
	require.True(t, i.End.Equal(expectedNow))
}
