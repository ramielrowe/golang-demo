package iso8601_interval

import (
	"encoding/json"
	"errors"
	"regexp"
	"strings"
	"time"

	"github.com/sosodev/duration"
)

var (
	iso8601IntervalStartEndRe *regexp.Regexp
	iso8601IntervalStartDurRe *regexp.Regexp
	iso8601IntervalDurEndRe   *regexp.Regexp
)

func init() {
	var err error
	// (timestamp(timezone) or (NOW()) / (timestamp(timezone)) or (NOW())
	iso8601IntervalStartEndRe, err = regexp.Compile(`^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:?\d{2}?)|NOW)\/(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:?\d{2}?)|NOW)$`)
	if err != nil {
		panic(err)
	}
	// (timestamp(timezone) or (NOW()) / (ISO8601 Duration)
	iso8601IntervalStartDurRe, err = regexp.Compile(`^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:?\d{2}?)|NOW)\/(P(\d+Y)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+S)?)?)$`)
	if err != nil {
		panic(err)
	}
	// (ISO8601 Duration + sub groups) / (timestamp(timezone) or (NOW())
	iso8601IntervalDurEndRe, err = regexp.Compile(`^(P(\d+Y)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+S)?)?)\/(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:?\d{2}?)|NOW)$`)
	if err != nil {
		panic(err)
	}
}

type ISO8601Interval struct {
	Start   time.Time
	End     time.Time
	timeNow func() time.Time
}

// Contains returns whether or not the specified time is between this interval's
func (i ISO8601Interval) Contains(t time.Time) bool {
	return (i.Start.Before(t) || i.Start.Equal(t)) && (i.End.After(t) || i.End.Equal(t))
}

func (i ISO8601Interval) now() time.Time {
	if i.timeNow != nil {
		return i.timeNow()
	}
	return time.Now()
}

func (i *ISO8601Interval) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	if iso8601IntervalStartEndRe.MatchString(s) {
		groups := iso8601IntervalStartEndRe.FindStringSubmatch(s)

		if strings.ToLower(groups[1]) == "now" {
			i.Start = i.now()
		} else {
			i.Start, err = time.Parse(time.RFC3339, groups[1])
			if err != nil {
				return err
			}
		}

		if strings.ToLower(groups[3]) == "now" {
			i.End = i.now()
		} else {
			i.End, err = time.Parse(time.RFC3339, groups[3])
			if err != nil {
				return err
			}
		}
		return nil
	}

	if iso8601IntervalStartDurRe.MatchString(s) {
		groups := iso8601IntervalStartDurRe.FindStringSubmatch(s)

		if strings.ToLower(groups[1]) == "now" {
			i.Start = i.now()
		} else {
			i.Start, err = time.Parse(time.RFC3339, groups[1])
			if err != nil {
				return err
			}
		}

		endDuration, err := duration.Parse(groups[3])
		if err != nil {
			return err
		}
		i.End = i.Start.Add(endDuration.ToTimeDuration())
		return nil
	}

	if iso8601IntervalDurEndRe.MatchString(s) {
		groups := iso8601IntervalDurEndRe.FindStringSubmatch(s)

		if strings.ToLower(groups[9]) == "now" {
			i.End = i.now()
		} else {
			i.End, err = time.Parse(time.RFC3339, groups[9])
			if err != nil {
				return err
			}
		}

		startDuration, err := duration.Parse(groups[1])
		if err != nil {
			return err
		}
		i.Start = i.End.Add(-startDuration.ToTimeDuration())
		return nil
	}

	return errors.New("string did not match any known ISO8601 Interval format")
}
