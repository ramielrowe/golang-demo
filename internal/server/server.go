package server

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	tomb "github.com/dharmab/tomb/v2"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/ramielrowe/golang-demo/internal/common_trace"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	otel_prom "go.opentelemetry.io/otel/exporters/prometheus"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.19.0"
	"go.uber.org/zap"
)

var (
	ErrServerStopped = errors.New("server stopped")
)

type Controller interface {
	Start(tomb *tomb.Tomb) error
}

type ServerConfig struct {
	HTTPAddr         string
	OtelServiceName  string
	OtelGRPCEndpoint string
	OtelGRPCInsecure bool
}

type Server struct {
	Config         ServerConfig
	Tomb           *tomb.Tomb
	MuxRouter      *mux.Router
	Logger         *otelzap.Logger
	TracerProvider *trace.TracerProvider
	MeterProvider  *metric.MeterProvider

	shuttingDown atomic.Bool

	httpServer  *http.Server
	controllers []Controller
}

func (s *Server) RegisterController(ctrl Controller) {
	s.controllers = append(s.controllers, ctrl)
}

func (s *Server) Start() {
	s.Tomb.Go(
		func() error {
			err := s.httpServer.ListenAndServe()
			if s.shuttingDown.Load() {
				// If the Server has started shutting down,
				// allow the Stop function to handle killing the
				// tomb after the HTTP server has fully stopped.
				return nil
			}
			return err
		},
	)
	for i := range s.controllers {
		s.controllers[i].Start(s.Tomb)
	}
	go func() {
		// Signal handling should be the last step and shouldn't be
		// part of the Server's tomb.
		sigChan := make(chan os.Signal, 2)
		signal.Notify(sigChan, syscall.SIGTERM, os.Interrupt)
		<-sigChan
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		s.Stop(ctx)
	}()
}

func (s *Server) Stop(ctx context.Context) {
	if s.shuttingDown.CompareAndSwap(false, true) {
		s.shuttingDown.Store(true)
		s.httpServer.Shutdown(ctx)
		s.Tomb.Kill(ErrServerStopped)
		s.Tomb.Wait()
	}
}

func (s *Server) Wait() error {
	return s.Tomb.Wait()
}

func newGRPCTracerExporter(config ServerConfig) *otlptrace.Exporter {
	opts := []otlptracegrpc.Option{otlptracegrpc.WithEndpoint(config.OtelGRPCEndpoint)}
	if config.OtelGRPCInsecure {
		opts = append(opts, otlptracegrpc.WithInsecure())
	}
	traceExporter, err := otlptracegrpc.New(context.Background(), opts...)
	if err != nil {
		panic(fmt.Sprintf("failed creating grpc trace exporter: %s", err.Error()))
	}
	return traceExporter
}

func newSTDOutTrace() *stdouttrace.Exporter {
	traceExporter, err := stdouttrace.New()
	if err != nil {
		panic(fmt.Sprintf("failed creating stdout trace exporter: %s", err.Error()))
	}
	return traceExporter
}

func NewServer(config ServerConfig) *Server {
	var traceExporter trace.SpanExporter
	if config.OtelGRPCEndpoint != "" {
		traceExporter = newGRPCTracerExporter(config)
	} else {
		traceExporter = newSTDOutTrace()
	}
	tracerProvider := trace.NewTracerProvider(
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithBatcher(traceExporter),
		trace.WithResource(resource.NewWithAttributes(semconv.SchemaURL, semconv.ServiceName(config.OtelServiceName))))

	metricRegistry := prometheus.NewRegistry()
	metricExporter, err := otel_prom.New(otel_prom.WithRegisterer(metricRegistry))
	if err != nil {
		panic(fmt.Sprintf("failed creating metric exporter: %s", err.Error()))
	}
	meterProvider := metric.NewMeterProvider(metric.WithReader(metricExporter))

	zapLogger, err := zap.NewProduction()
	if err != nil {
		panic(fmt.Sprintf("failed creating logger: %s", err.Error()))
	}
	logger := otelzap.New(zapLogger)

	muxRouter := mux.NewRouter()
	muxRouter.Use(otelmux.Middleware(
		config.OtelServiceName,
		otelmux.WithTracerProvider(tracerProvider),
		otelmux.WithSpanNameFormatter(common_trace.OTELMuxSpanName)))

	muxRouter.Path("/metrics").Handler(promhttp.HandlerFor(metricRegistry, promhttp.HandlerOpts{Registry: metricRegistry}))

	httpServer := &http.Server{
		Addr:    config.HTTPAddr,
		Handler: muxRouter,
	}
	return &Server{
		Config:         config,
		Tomb:           &tomb.Tomb{},
		MuxRouter:      muxRouter,
		Logger:         logger,
		TracerProvider: tracerProvider,
		MeterProvider:  meterProvider,
		httpServer:     httpServer,
	}
}
