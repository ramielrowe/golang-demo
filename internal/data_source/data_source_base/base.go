package data_source_base

import (
	"context"
	"fmt"
	"math"
)

type DataSourceName string

const (
	// DataSourceNameMock is used in Unit Testing
	DataSourceNameMock = "mock/v1"
	// DataSourceNameNoop is used in E2E Testing, it no-ops the Weather Observation process
	DataSourceNameNoop = "noop/v1"
)

type WeatherDescription string

func (d WeatherDescription) Valid() bool {
	_, ok := WeatherDescriptionPriority[d]
	return ok
}

const (
	WeatherClear        WeatherDescription = "clear"
	WeatherClouds       WeatherDescription = "clouds"
	WeatherFog          WeatherDescription = "fog"
	WeatherRain         WeatherDescription = "rain"
	WeatherFreezingRain WeatherDescription = "freezing_rain"
	WeatherSnow         WeatherDescription = "snow"
	WeatherThunderstorm WeatherDescription = "thunderstorm"
	WeatherExtreme      WeatherDescription = "extreme"
	WeatherUnknown      WeatherDescription = "unknown"
)

var (
	// WeatherDescriptionPriority determines precedence of WeatherDescriptions
	//   should there be more than one description for a time period.
	WeatherDescriptionPriority = map[WeatherDescription]int64{
		WeatherUnknown:      0,
		WeatherClear:        1,
		WeatherClouds:       2,
		WeatherFog:          3,
		WeatherRain:         4,
		WeatherThunderstorm: 5,
		WeatherSnow:         6,
		WeatherFreezingRain: 7,
		WeatherExtreme:      math.MaxInt64, // Highest
	}
)

type WeatherObservation struct {
	CurrentDescription WeatherDescription
	CurrentMinTemp     float64
	CurrentMaxTemp     float64
}

type DataSourceErr interface {
	error
	Retryable() bool
}

type ErrUnexpectedStatusRetryable struct {
	StatusCode int64
}

var _ DataSourceErr = ErrUnexpectedStatusRetryable{}

func (e ErrUnexpectedStatusRetryable) Error() string {
	return fmt.Sprintf("retryable unexpected status code encountered: %d", e.StatusCode)
}

func (e ErrUnexpectedStatusRetryable) Retryable() bool {
	return true
}

type ErrUnexpectedStatusFatal struct {
	StatusCode int64
}

var _ DataSourceErr = ErrUnexpectedStatusFatal{}

func (e ErrUnexpectedStatusFatal) Error() string {
	return fmt.Sprintf("fatal unexpected status code encountered: %d", e.StatusCode)
}

func (e ErrUnexpectedStatusFatal) Retryable() bool {
	return false
}

type ErrUnexpectedErrorRetryable struct {
	Err error
}

var _ DataSourceErr = ErrUnexpectedErrorRetryable{}

func (e ErrUnexpectedErrorRetryable) Error() string {
	return fmt.Sprintf("retryable error encountered: %s", e.Err.Error())
}

func (e ErrUnexpectedErrorRetryable) Retryable() bool {
	return true
}

type ErrUnexpectedErrorFatal struct {
	Err error
}

var _ DataSourceErr = ErrUnexpectedErrorFatal{}

func (e ErrUnexpectedErrorFatal) Error() string {
	return fmt.Sprintf("retryable error encountered: %s", e.Err.Error())
}

func (e ErrUnexpectedErrorFatal) Retryable() bool {
	return false
}

type DataSource interface {
	GetCurrentWeather(ctx context.Context, longitude float64, latitude float64) (*WeatherObservation, error)
}
