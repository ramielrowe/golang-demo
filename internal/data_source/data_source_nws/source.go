package data_source_nws

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/floatdrop/lru"
	base "gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/default_http"
	"gitlab.com/ramielrowe/golang-demo/internal/iso8601_interval"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

const (
	DataSourceNameNWS_v1 = base.DataSourceName("nws/v1")
	DefaultEndpoint      = "https://api.weather.gov"
)

type WeatherDescription string

const (
	WeatherBlowingDust     WeatherDescription = "blowing_dust"
	WeatherBlowingSand     WeatherDescription = "blowing_sand"
	WeatherBlowingSnow     WeatherDescription = "blowing_snow"
	WeatherDrizzle         WeatherDescription = "drizzle"
	WeatherFog             WeatherDescription = "fog"
	WeatherFreezingFog     WeatherDescription = "freezing_fog"
	WeatherFreezingDrizzle WeatherDescription = "freezing_drizzle"
	WeatherFreezingRain    WeatherDescription = "freezing_rain"
	WeatherFreezingSpray   WeatherDescription = "freezing_spray"
	WeatherFrost           WeatherDescription = "frost"
	WeatherHail            WeatherDescription = "hail"
	WeatherHaze            WeatherDescription = "haze"
	WeatherIceCrystals     WeatherDescription = "ice_crystals"
	WeatherIceFog          WeatherDescription = "ice_fog"
	WeatherRain            WeatherDescription = "rain"
	WeatherRainShowers     WeatherDescription = "rain_showers"
	WeatherSleet           WeatherDescription = "sleet"
	WeatherSmoke           WeatherDescription = "smoke"
	WeatherSnow            WeatherDescription = "snow"
	WeatherSnowShowers     WeatherDescription = "snow_showers"
	WeatherThunderstorms   WeatherDescription = "thunderstorms"
	WeatherVolcanicAsh     WeatherDescription = "volcanic_ash"
	WeatherWaterSpouts     WeatherDescription = "water_spouts"
)

var (
	WeatherDescriptionMap = map[WeatherDescription]base.WeatherDescription{
		WeatherBlowingDust:     base.WeatherExtreme,
		WeatherBlowingSand:     base.WeatherExtreme,
		WeatherBlowingSnow:     base.WeatherExtreme,
		WeatherDrizzle:         base.WeatherRain,
		WeatherFog:             base.WeatherFog,
		WeatherFreezingFog:     base.WeatherFog,
		WeatherFreezingDrizzle: base.WeatherFreezingRain,
		WeatherFreezingRain:    base.WeatherFreezingRain,
		WeatherFreezingSpray:   base.WeatherFreezingRain,
		WeatherFrost:           base.WeatherFreezingRain,
		WeatherHail:            base.WeatherExtreme,
		WeatherHaze:            base.WeatherFog,
		WeatherIceCrystals:     base.WeatherFreezingRain,
		WeatherIceFog:          base.WeatherFog,
		WeatherRain:            base.WeatherRain,
		WeatherRainShowers:     base.WeatherRain,
		WeatherSleet:           base.WeatherSnow,
		WeatherSmoke:           base.WeatherExtreme,
		WeatherSnow:            base.WeatherSnow,
		WeatherSnowShowers:     base.WeatherSnow,
		WeatherThunderstorms:   base.WeatherThunderstorm,
		WeatherVolcanicAsh:     base.WeatherExtreme,
		WeatherWaterSpouts:     base.WeatherExtreme,
	}
)

type lonLatTuple struct {
	lon float64
	lat float64
}

type pointProperties struct {
	OfficeCode string `json:"cwa"`
	GridX      int64  `json:"gridX"`
	GridY      int64  `json:"gridY"`
}

type point struct {
	ID         string          `json:"id"`
	Properties pointProperties `json:"properties"`
}

type weather struct {
	Weather *WeatherDescription `json:"weather"`
}

type weatherData struct {
	ValidTime iso8601_interval.ISO8601Interval `json:"validTime"`
	Value     []weather                        `json:"value"`
}

type weatherProperties struct {
	Values []weatherData `json:"values"`
}

type temperatureData struct {
	ValidTime iso8601_interval.ISO8601Interval `json:"validTime"`
	Value     float64                          `json:"value"`
}

type temperatureProperties struct {
	Values []temperatureData `json:"values"`
}

type gridPointProperties struct {
	Weather        weatherProperties     `json:"weather"`
	MinTemperature temperatureProperties `json:"minTemperature"`
	MaxTemperature temperatureProperties `json:"maxTemperature"`
}

type gridPoint struct {
	ID         string              `json:"id"`
	Properties gridPointProperties `json:"properties"`
}

func gridPointToWeatherObservation(now time.Time, gp *gridPoint) *base.WeatherObservation {
	var currentWeather base.WeatherObservation

	currentWeather.CurrentDescription = base.WeatherUnknown
	for _, weatherProps := range gp.Properties.Weather.Values {
		if weatherProps.ValidTime.Contains(now) {
			weatherDesc := base.WeatherUnknown
			priority := base.WeatherDescriptionPriority[weatherDesc]
			for _, weather := range weatherProps.Value {
				mappedDesc := base.WeatherClear
				if weather.Weather != nil {
					var ok bool
					mappedDesc, ok = WeatherDescriptionMap[*weather.Weather]
					if !ok {
						mappedDesc = base.WeatherUnknown
					}
				}

				mappedPriority := base.WeatherDescriptionPriority[mappedDesc]
				if mappedPriority > priority {
					weatherDesc = mappedDesc
				}
			}
			currentWeather.CurrentDescription = weatherDesc
			break
		}
	}

	for _, tempProps := range gp.Properties.MinTemperature.Values {
		y, m, d := tempProps.ValidTime.Start.Date()
		nowY, nowM, nowD := now.Date()
		if y == nowY && m == nowM && d == nowD {
			currentWeather.CurrentMinTemp = tempProps.Value
			break
		}
	}

	for _, tempProps := range gp.Properties.MaxTemperature.Values {
		y, m, d := tempProps.ValidTime.Start.Date()
		nowY, nowM, nowD := now.Date()
		if y == nowY && m == nowM && d == nowD {
			currentWeather.CurrentMaxTemp = tempProps.Value
			break
		}
	}
	return &currentWeather
}

type NWSSource struct {
	tracer     trace.Tracer
	endpoint   string
	http       *http.Client
	pointCache *lru.LRU[lonLatTuple, point]
	nowTime    func() time.Time
}

var _ base.DataSource = (*NWSSource)(nil)

func NewDefaultNWSSource(tracerProvider trace.TracerProvider) *NWSSource {
	return &NWSSource{
		tracer:     tracerProvider.Tracer("nws_source"),
		endpoint:   DefaultEndpoint,
		http:       default_http.NewClient(),
		pointCache: lru.New[lonLatTuple, point](256),
	}
}

// now allows tests to inject a controlled time function
func (c NWSSource) now() time.Time {
	if c.nowTime != nil {
		return c.nowTime()
	}
	return time.Now()
}

func (c *NWSSource) getPoint(ctx context.Context, longitude float64, latitude float64) (*point, error) {
	ctx, span := c.tracer.Start(ctx, "NWSSource.getPoint")
	defer span.End()
	span.SetAttributes(attribute.Float64("longitude", longitude))
	span.SetAttributes(attribute.Float64("latitude", latitude))
	span.SetAttributes(attribute.Bool("from_cache", false))

	cacheKey := lonLatTuple{longitude, latitude}

	cachePoint := c.pointCache.Get(cacheKey)
	if cachePoint != nil {
		span.SetAttributes(attribute.Bool("from_cache", true))
		return cachePoint, nil
	}

	url := fmt.Sprintf("%s/points/%f,%f", c.endpoint, latitude, longitude)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed building request")
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed calling API")
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		var err error
		err = base.ErrUnexpectedStatusRetryable{StatusCode: int64(resp.StatusCode)}
		if resp.StatusCode < 500 {
			err = base.ErrUnexpectedStatusFatal{StatusCode: int64(resp.StatusCode)}
		}
		span.RecordError(err)
		span.SetStatus(codes.Error, "Unexpected status code from API")
		return nil, err
	}
	var p point
	err = json.NewDecoder(resp.Body).Decode(&p)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed decoding response from API")
		return nil, base.ErrUnexpectedErrorRetryable{Err: err}
	}
	c.pointCache.Set(cacheKey, p)
	return &p, nil
}

func (c *NWSSource) getGridPoint(ctx context.Context, office string, gridX int64, gridY int64) (*gridPoint, error) {
	ctx, span := c.tracer.Start(ctx, "NWSSource.getGridPoint")
	defer span.End()
	span.SetAttributes(attribute.String("office", office))
	span.SetAttributes(attribute.Int64("gridX", gridX))
	span.SetAttributes(attribute.Int64("gridY", gridY))

	url := fmt.Sprintf("%s/gridpoints/%s/%d,%d", c.endpoint, office, gridX, gridY)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed building request")
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed calling API")
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		var err error
		err = base.ErrUnexpectedStatusRetryable{StatusCode: int64(resp.StatusCode)}
		if resp.StatusCode < 500 {
			err = base.ErrUnexpectedStatusFatal{StatusCode: int64(resp.StatusCode)}
		}
		span.RecordError(err)
		span.SetStatus(codes.Error, "Unexpected status code from API")
		return nil, err
	}

	var gp gridPoint
	err = json.NewDecoder(resp.Body).Decode(&gp)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed decoding response from API")
		return nil, base.ErrUnexpectedErrorRetryable{Err: err}
	}
	return &gp, nil
}

func (c *NWSSource) GetCurrentWeather(ctx context.Context, longitude float64, latitude float64) (*base.WeatherObservation, error) {
	ctx, span := c.tracer.Start(ctx, "NWSSource.GetCurrentWeather")
	defer span.End()
	span.SetAttributes(attribute.Float64("longitude", longitude))
	span.SetAttributes(attribute.Float64("latitude", latitude))

	point, err := c.getPoint(ctx, longitude, latitude)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed getting Point for location")
		return nil, err
	}
	gridPoint, err := c.getGridPoint(ctx, point.Properties.OfficeCode, point.Properties.GridX, point.Properties.GridY)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed getting GridPoint for location")
		return nil, err
	}
	return gridPointToWeatherObservation(c.now(), gridPoint), nil
}
