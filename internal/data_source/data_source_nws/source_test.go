package data_source_nws

import (
	"context"
	"net/http"
	"net/http/httptest"
	"regexp"
	"sync/atomic"
	"testing"
	"time"

	"github.com/floatdrop/lru"
	"github.com/stretchr/testify/suite"
	base "gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/default_http"
	"go.opentelemetry.io/otel"
)

type FakeNWSServer struct {
	requests      atomic.Int64
	pointResp     string
	gridPointResp string
}

func (s *FakeNWSServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	pointPathRe, err := regexp.Compile(`^/points/[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+),[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$`)
	if err != nil {
		panic(err)
	}
	if pointPathRe.MatchString(r.URL.Path) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(s.pointResp))
		s.requests.Add(1)
		return
	}
	gridPointPathRe, err := regexp.Compile(`^/gridpoints/([a-zA-Z]+)/(\d+),(\d+)$`)
	if err != nil {
		panic(err)
	}
	if gridPointPathRe.MatchString(r.URL.Path) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(s.gridPointResp))
		s.requests.Add(1)
		return
	}
}

type NWSClientTestSuite struct {
	suite.Suite

	fakeNWS *FakeNWSServer
	srv     *httptest.Server
	source  *NWSSource
}

func (s *NWSClientTestSuite) SetupTest() {
	s.fakeNWS = &FakeNWSServer{
		pointResp:     fakePoint,
		gridPointResp: fakeGridPoint,
	}
	s.srv = httptest.NewServer(s.fakeNWS)
	s.source = &NWSSource{
		tracer:     otel.GetTracerProvider().Tracer("nws_source"),
		endpoint:   s.srv.URL,
		http:       default_http.NewClient(),
		pointCache: lru.New[lonLatTuple, point](256),
	}

}

func (s *NWSClientTestSuite) TestGetPoint() {
	ctx := context.Background()

	point, err := s.source.getPoint(ctx, -75.97799, 36.85293)
	s.Require().NoError(err)
	s.Require().NotNil(point)
	s.Require().Equal("AKQ", point.Properties.OfficeCode)
	s.Require().Equal(int64(101), point.Properties.GridX)
	s.Require().Equal(int64(53), point.Properties.GridY)
	s.Require().Equal(int64(1), s.fakeNWS.requests.Load())

	cachePoint := s.source.pointCache.Get(lonLatTuple{-75.97799, 36.85293})
	s.Require().NotNil(cachePoint)
	s.Require().Equal("AKQ", cachePoint.Properties.OfficeCode)
	s.Require().Equal(int64(101), cachePoint.Properties.GridX)
	s.Require().Equal(int64(53), cachePoint.Properties.GridY)

	point, err = s.source.getPoint(ctx, -75.97799, 36.85293)
	s.Require().NoError(err)
	s.Require().NotNil(point)
	s.Require().Equal("AKQ", point.Properties.OfficeCode)
	s.Require().Equal(int64(101), point.Properties.GridX)
	s.Require().Equal(int64(53), point.Properties.GridY)
	s.Require().Equal(int64(1), s.fakeNWS.requests.Load())
}

func (s *NWSClientTestSuite) TestGetCurrentWeather() {
	ctx := context.Background()
	s.source.nowTime = func() time.Time {
		return fakeGridPointTime
	}

	weather, err := s.source.GetCurrentWeather(ctx, -75.97799, 36.85293)
	s.Require().NoError(err)
	s.Require().Equal(base.WeatherRain, weather.CurrentDescription)
	s.Require().Equal(18.333333333333332, weather.CurrentMinTemp)
	s.Require().Equal(21.11111111111111, weather.CurrentMaxTemp)
}

func (s *NWSClientTestSuite) TestGetCurrentWeatherClear() {
	s.fakeNWS.gridPointResp = fakeGridPointClear

	ctx := context.Background()
	s.source.nowTime = func() time.Time {
		return fakeGridPointClearTime
	}

	weather, err := s.source.GetCurrentWeather(ctx, -75.97799, 36.85293)
	s.Require().NoError(err)
	s.Require().Equal(base.WeatherClear, weather.CurrentDescription)
	s.Require().Equal(17.77777777777778, weather.CurrentMinTemp)
	s.Require().Equal(23.88888888888889, weather.CurrentMaxTemp)
}

func TestNWSClientTestSuite(t *testing.T) {
	suite.Run(t, new(NWSClientTestSuite))
}
