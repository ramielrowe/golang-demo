package data_source_mock

import (
	base "gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
)

const (
	DataSourceNameMock_v1 = base.DataSourceName("mock/v1")
)
