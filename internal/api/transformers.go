package api

import (
	"time"

	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/store"
)

func formatTime(t time.Time) string {
	return t.Format(time.RFC3339)
}

func storeLocationMonitorToAPI(m *store.LocationMonitor) api_gen.LocationMonitor {
	return api_gen.LocationMonitor{
		Uuid:                      m.UUID.String(),
		CreatedAt:                 formatTime(m.CreatedAt),
		UpdatedAt:                 formatTime(m.UpdatedAt),
		Name:                      m.Name,
		DataSource:                api_gen.LocationMonitorDataSource(m.DataSource),
		Longitude:                 m.Longitude,
		Latitude:                  m.Latitude,
		Status:                    api_gen.LocationMonitorStatus(m.Status),
		CurrentWeatherDescription: api_gen.WeatherDescription(m.CurrentWeatherDescription),
		CurrentMaxTemp:            m.CurrentMaxTemp,
		CurrentMinTemp:            m.CurrentMinTemp,
	}
}

func apiNewLocationMonitorToStore(m api_gen.NewLocationMonitor) store.NewLocationMonitor {
	name := ""
	if m.Name != nil {
		name = *m.Name
	}
	return store.NewLocationMonitor{
		Name:       name,
		DataSource: data_source_base.DataSourceName(m.DataSource), // TODO: implement actual conversion check/error
		Longitude:  m.Longitude,
		Latitude:   m.Latitude,
	}
}

func apiObservationToDataSource(apiObservation api_gen.WeatherObservation) (data_source_base.WeatherObservation, bool) {
	dataSourceObservation := data_source_base.WeatherObservation{
		CurrentMinTemp:     apiObservation.CurrentMinTemp,
		CurrentMaxTemp:     apiObservation.CurrentMaxTemp,
		CurrentDescription: data_source_base.WeatherDescription(apiObservation.CurrentWeatherDescription),
	}
	if !dataSourceObservation.CurrentDescription.Valid() {
		return data_source_base.WeatherObservation{}, false
	}
	return dataSourceObservation, true
}
