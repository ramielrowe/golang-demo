package api

type ErrorCode string

const (
	UnknownErrorCode ErrorCode = "unknown"
	BadRequestCode   ErrorCode = "bad_request"
	NotFoundCode     ErrorCode = "not_found"
)
