package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_events"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/store"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type APIServer struct {
	store        store.LocationMonitorStore
	eventHandler api_events.EventHandler
}

var _ api_gen.ServerInterface = (*APIServer)(nil)

func NewAPIServer(store store.LocationMonitorStore, eventHandler api_events.EventHandler) *APIServer {
	return &APIServer{store: store, eventHandler: eventHandler}
}

// writeJSON marshals obj to JSON and writes it to the client
func (s *APIServer) writeJSON(ctx context.Context, w http.ResponseWriter, status int, obj interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	err := json.NewEncoder(w).Encode(obj)
	if err != nil {
		trace.SpanFromContext(ctx).AddEvent("response writer error", trace.WithAttributes(attribute.String("error", err.Error())))
	}
}

// writeError writes an ErrorResponse to the client
func (s *APIServer) writeError(ctx context.Context, w http.ResponseWriter, status int, code ErrorCode, msg string) {
	resp := api_gen.ErrorResponse{
		Error: api_gen.Error{
			Code:    string(code),
			Message: msg,
		},
	}
	s.writeJSON(ctx, w, status, resp)
}

// (GET /locations)
func (s *APIServer) ListLocationMonitors(w http.ResponseWriter, r *http.Request, params api_gen.ListLocationMonitorsParams) {
	ctx := r.Context()
	opts := store.ListLocationMonitorsOptions{
		PaginationToken: params.PageToken,
	}
	page, err := s.store.ListLocationMonitors(ctx, opts)
	if err != nil {
		trace.SpanFromContext(ctx).RecordError(err)
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, "unknown")
		return
	}

	resp := api_gen.ListLocationMonitorsResponseBody{
		NextPageToken:    page.PaginationToken,
		LocationMonitors: make([]api_gen.LocationMonitor, len(page.LocationMonitors)),
	}
	for i := 0; i < len(page.LocationMonitors); i++ {
		resp.LocationMonitors[i] = storeLocationMonitorToAPI(page.LocationMonitors[i])
	}

	s.writeJSON(ctx, w, http.StatusOK, resp) // TODO: handle error return
}

// (POST /locations)
func (s *APIServer) CreateLocationMonitor(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req api_gen.CreateLocationMonitorRequestBody
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed Location Monitor")
		return
	}

	newLM := apiNewLocationMonitorToStore(req.LocationMonitor)
	newLM.UUID = uuid.New()

	opts := store.CreateLocationMonitorOptions{Callback: s.eventHandler.LocationMonitorCreated}
	err = s.store.CreateLocationMonitor(ctx, newLM, opts)
	if err != nil {
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, err.Error())
		return
	}

	lm, err := s.store.GetLocationMonitor(ctx, newLM.UUID)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed Location Monitor")
		return
	}

	if lm == nil {
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, "unknown error occurred")
		return
	}

	resp := api_gen.GetLocationMonitorResponseBody{
		LocationMonitor: storeLocationMonitorToAPI(lm),
	}
	s.writeJSON(ctx, w, http.StatusOK, resp)
}

// (GET /locations/{location-id})
func (s *APIServer) GetLocationMonitor(w http.ResponseWriter, r *http.Request, locationId string) {
	ctx := r.Context()
	locationUUID, err := uuid.Parse(locationId)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed location UUID")
		return
	}
	lm, err := s.store.GetLocationMonitor(ctx, locationUUID)
	if err != nil {
		if err == store.ErrRecordNotFound {
			s.writeError(ctx, w, http.StatusNotFound, NotFoundCode, fmt.Sprintf("LocationMonitor %s not found", locationId))
			return
		}
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, err.Error())
		return
	}

	if lm == nil {
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, "unknown error occurred")
		return
	}

	resp := api_gen.GetLocationMonitorResponseBody{
		LocationMonitor: storeLocationMonitorToAPI(lm),
	}
	s.writeJSON(ctx, w, http.StatusOK, resp)
}

// (POST /locations/{location-id}/observations)
func (s *APIServer) RecordWeatherObservation(w http.ResponseWriter, r *http.Request, locationId string) {
	ctx := r.Context()

	locationUUID, err := uuid.Parse(locationId)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, fmt.Sprintf("Malformed location UUID: %s", locationId))
		return
	}

	var req api_gen.RecordWeatherObservationRequestBody
	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed Location Monitor")
		return
	}

	sourceObservation, ok := apiObservationToDataSource(req.Observation)
	if !ok {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed Weather Observation")
		return
	}

	// Ensure location monitor exists
	_, err = s.store.GetLocationMonitor(ctx, locationUUID)
	if err != nil {
		if err == store.ErrRecordNotFound {
			s.writeError(ctx, w, http.StatusNotFound, NotFoundCode, fmt.Sprintf("LocationMonitor %s not found", locationId))
			return
		}
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, err.Error())
		return
	}

	err = s.store.RecordWeatherObservation(ctx, locationUUID, sourceObservation)

	if err != nil {
		if err == store.ErrRecordNotFound {
			s.writeError(ctx, w, http.StatusNotFound, NotFoundCode, fmt.Sprintf("LocationMonitor %s not found", locationId))
			return
		}
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
}

// (Delete /locations/{location-id})
func (s *APIServer) DeleteLocationMonitor(w http.ResponseWriter, r *http.Request, locationId string) {
	ctx := r.Context()
	locationUUID, err := uuid.Parse(locationId)
	if err != nil {
		s.writeError(ctx, w, http.StatusBadRequest, BadRequestCode, "Malformed location UUID")
		return
	}
	err = s.store.DeleteLocationMonitor(ctx, locationUUID)
	if err != nil {
		if err == store.ErrRecordNotFound {
			s.writeError(ctx, w, http.StatusNotFound, NotFoundCode, fmt.Sprintf("LocationMonitor %s not found", locationId))
			return
		}
		s.writeError(ctx, w, http.StatusInternalServerError, UnknownErrorCode, err.Error())
		return
	}
	w.WriteHeader(http.StatusOK)
}
