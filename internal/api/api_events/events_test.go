package api_events

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

func TestLocationMonitorCreated(t *testing.T) {
	expectedEvent := Event[LocationMonitorCreated]{
		Type:      EventTypeLocationMonitorCreated,
		Timestamp: time.Now(),
		Body: LocationMonitorCreated{
			UUID: uuid.New(),
		},
	}

	rawJSON, err := json.Marshal(expectedEvent)
	require.NoError(t, err)

	actualEvent, err := UnmarshalEvent[LocationMonitorCreated](rawJSON)
	require.NoError(t, err)
	require.Equal(t, expectedEvent.Type, actualEvent.Type)
	require.True(t, expectedEvent.Timestamp.Equal(actualEvent.Timestamp))
	require.Equal(t, expectedEvent.Body.UUID.String(), actualEvent.Body.UUID.String())
}
