package api_events

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type EventType string

const (
	EventTypeLocationMonitorCreated = "location_monitor_created"
)

type Event[b any] struct {
	Type      EventType `json:"type"`
	Timestamp time.Time `json:"timestamp"`
	Body      b         `json:"body"`
}

type LocationMonitorCreated struct {
	UUID uuid.UUID `json:"uuid"`
}

func UnmarshalEvent[b any](data []byte) (*Event[b], error) {
	var event Event[b]
	err := json.Unmarshal(data, &event)
	if err != nil {
		return nil, err
	}
	return &event, nil
}

type EventHandler interface {
	LocationMonitorCreated(ctx context.Context, id uuid.UUID) error
}

type NoopEventHandler struct{}

func (h *NoopEventHandler) LocationMonitorCreated(ctx context.Context, id uuid.UUID) error {
	return nil
}
