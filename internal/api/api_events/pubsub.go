package api_events

import (
	"context"
	"encoding/json"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

type PubSubEventHandler struct {
	tracer                      trace.Tracer
	locationMonitorCreatedTopic *pubsub.Topic
}

func NewPubSubEventHandler(tp trace.TracerProvider, lmcTopic *pubsub.Topic) *PubSubEventHandler {
	return &PubSubEventHandler{
		tracer:                      tp.Tracer("PubSubEventHandler"),
		locationMonitorCreatedTopic: lmcTopic,
	}
}

func (h *PubSubEventHandler) publishMsg(ctx context.Context, topic *pubsub.Topic, body []byte) error {
	msg := &pubsub.Message{Data: body, Attributes: make(map[string]string)}
	otel.GetTextMapPropagator().Inject(ctx, propagation.MapCarrier(msg.Attributes))
	serverID, err := topic.Publish(ctx, msg).Get(ctx)
	if serverID != "" {
		trace.SpanFromContext(ctx).SetAttributes(attribute.String("pubsub_server_id", serverID))
	}
	return err
}

func (h *PubSubEventHandler) LocationMonitorCreated(ctx context.Context, id uuid.UUID) error {
	ctx, span := h.tracer.Start(ctx, "PubSubEventHandler.LocationMonitorCreated")
	defer span.End()

	event := Event[LocationMonitorCreated]{
		Type:      EventTypeLocationMonitorCreated,
		Timestamp: time.Now(),
		Body: LocationMonitorCreated{
			UUID: id,
		},
	}

	rawEvent, err := json.Marshal(event)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed marshalling event to JSON")
		return err
	}

	err = h.publishMsg(ctx, h.locationMonitorCreatedTopic, rawEvent)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed publishing event")
		return err
	}
	return nil
}
