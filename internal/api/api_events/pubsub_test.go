package api_events

import (
	"context"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/pubsub/pstest"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"go.opentelemetry.io/otel"
	"google.golang.org/api/option"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type PubSubEventHandlerTestSuite struct {
	suite.Suite

	lmcTopic *pubsub.Topic
	lmcSub   *pubsub.Subscription

	handler *PubSubEventHandler
}

func (s *PubSubEventHandlerTestSuite) SetupTest() {
	ctx := context.Background()

	srv := pstest.NewServer()
	defer srv.Close()

	conn, err := grpc.Dial(srv.Addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	s.Require().NoError(err)
	defer conn.Close()

	client, err := pubsub.NewClient(ctx, "fakeproject", option.WithGRPCConn(conn))
	s.Require().NoError(err)
	defer client.Close()

	s.lmcTopic, err = client.CreateTopic(ctx, EventTypeLocationMonitorCreated)
	s.Require().NoError(err)
	s.lmcSub, err = client.CreateSubscription(ctx, EventTypeLocationMonitorCreated, pubsub.SubscriptionConfig{Topic: s.lmcTopic})
	s.Require().NoError(err)

	s.handler = NewPubSubEventHandler(otel.GetTracerProvider(), s.lmcTopic)
}

func (s *PubSubEventHandlerTestSuite) TestLocationMonitorCreated() {
	ctx := context.Background()

	id := uuid.New()
	s.Require().NoError(s.handler.LocationMonitorCreated(ctx, id))

	ctx, cancel := context.WithCancel(ctx)
	s.lmcSub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		event, err := UnmarshalEvent[LocationMonitorCreated](msg.Data)
		s.Require().NoError(err)
		s.Require().Equal(EventTypeLocationMonitorCreated, event.Type)
		s.Require().False(event.Timestamp.IsZero())
		s.Require().Equal(id.String(), event.Body.UUID.String())
		cancel()
	})
}
