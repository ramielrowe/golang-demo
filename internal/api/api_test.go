package api

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_events"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_nws"
	"gitlab.com/ramielrowe/golang-demo/internal/store"
	"gitlab.com/ramielrowe/golang-demo/internal/store/store_mocks"
	"go.uber.org/mock/gomock"
)

type APITestSuite struct {
	suite.Suite

	store  *store_mocks.MockLocationMonitorStore
	server *httptest.Server
}

func (s *APITestSuite) SetupTest() {
	mockCtrl := gomock.NewController(s.T())
	s.store = store_mocks.NewMockLocationMonitorStore(mockCtrl)

	apiServer := NewAPIServer(s.store, &api_events.NoopEventHandler{})
	s.server = httptest.NewServer(api_gen.HandlerWithOptions(apiServer, api_gen.GorillaServerOptions{}))
}

func (s *APITestSuite) TearDownTest() {
	s.server.Close()
}

func (s *APITestSuite) postObject(path string, obj interface{}) (*http.Response, error) {
	buf := bytes.NewBuffer(nil)
	err := json.NewEncoder(buf).Encode(obj)
	if err != nil {
		return nil, err
	}

	return s.server.Client().Post(s.server.URL+path, "application/json", buf)
}

func (s *APITestSuite) get(path string, query map[string]string) (*http.Response, error) {
	queryValues := make(url.Values)
	for key, value := range query {
		queryValues.Add(key, value)
	}
	url := s.server.URL + path
	if len(queryValues) > 0 {
		url += "?" + queryValues.Encode()
	}
	return s.server.Client().Get(url)
}

func (s *APITestSuite) delete(path string) (*http.Response, error) {
	url := s.server.URL + path
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, err
	}
	return s.server.Client().Do(req)
}

func (s *APITestSuite) TestCreateLocationMonitor() {
	name := "Virginia Beach, Virginia"
	req := api_gen.CreateLocationMonitorRequestBody{
		LocationMonitor: api_gen.NewLocationMonitor{
			Name:       &name,
			DataSource: api_gen.NewLocationMonitorDataSourceNwsv1,
			Longitude:  -75.97799,
			Latitude:   36.85293,
		},
	}

	s.store.EXPECT().CreateLocationMonitor(gomock.Any(), gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, newMonitor store.NewLocationMonitor, opt store.CreateLocationMonitorOptions) error {
			s.Require().Equal(uuid.RFC4122, newMonitor.UUID.Variant())
			s.Require().Equal(*req.LocationMonitor.Name, newMonitor.Name)
			return nil
		})

	var expectedUUID uuid.UUID
	s.store.EXPECT().GetLocationMonitor(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, id uuid.UUID) (*store.LocationMonitor, error) {
			expectedUUID = id
			return &store.LocationMonitor{
				UUID:       id,
				CreatedAt:  time.Now(),
				UpdatedAt:  time.Now(),
				Name:       name,
				DataSource: data_source_nws.DataSourceNameNWS_v1,
				Longitude:  req.LocationMonitor.Longitude,
				Latitude:   req.LocationMonitor.Latitude,
				Status:     store.LocationMonitorStatusActive,
			}, nil
		})

	httpResp, err := s.postObject("/locations", req)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	resp, err := api_gen.ParseGetLocationMonitorResponse(httpResp)
	s.Require().NoError(err)
	s.Require().NotNil(resp.JSON200)

	s.Require().Equal(expectedUUID.String(), resp.JSON200.LocationMonitor.Uuid)
	s.Require().Equal(name, resp.JSON200.LocationMonitor.Name)
	s.Require().Equal(api_gen.LocationMonitorDataSourceNwsv1, resp.JSON200.LocationMonitor.DataSource)
	s.Require().Equal(req.LocationMonitor.Longitude, resp.JSON200.LocationMonitor.Longitude)
	s.Require().Equal(req.LocationMonitor.Latitude, resp.JSON200.LocationMonitor.Latitude)
	s.Require().Equal(api_gen.Active, resp.JSON200.LocationMonitor.Status)
}

func (s *APITestSuite) TestGetLocationMonitor() {
	currrentMin := 63.0
	currrentMax := 74.0
	expectedLocation := &store.LocationMonitor{
		UUID:                      uuid.New(),
		CreatedAt:                 time.Now(),
		UpdatedAt:                 time.Now(),
		Name:                      "Virginia Beach, Virginia",
		DataSource:                data_source_nws.DataSourceNameNWS_v1,
		Longitude:                 -75.97799,
		Latitude:                  36.85293,
		Status:                    store.LocationMonitorStatusActive,
		CurrentWeatherDescription: data_source_base.WeatherUnknown,
		CurrentMinTemp:            &currrentMin,
		CurrentMaxTemp:            &currrentMax,
	}
	s.store.EXPECT().GetLocationMonitor(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, id uuid.UUID) (*store.LocationMonitor, error) {
			s.Require().Equal(expectedLocation.UUID.String(), id.String())
			return expectedLocation, nil
		})

	httpResp, err := s.get(fmt.Sprintf("/locations/%s", expectedLocation.UUID.String()), nil)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	resp, err := api_gen.ParseGetLocationMonitorResponse(httpResp)
	s.Require().NoError(err)
	s.Require().NotNil(resp.JSON200)

	s.Require().Equal(expectedLocation.UUID.String(), resp.JSON200.LocationMonitor.Uuid)
	s.Require().Equal(expectedLocation.Name, resp.JSON200.LocationMonitor.Name)
	s.Require().Equal(api_gen.LocationMonitorDataSourceNwsv1, resp.JSON200.LocationMonitor.DataSource)
	s.Require().Equal(expectedLocation.Longitude, resp.JSON200.LocationMonitor.Longitude)
	s.Require().Equal(expectedLocation.Latitude, resp.JSON200.LocationMonitor.Latitude)
	s.Require().Equal(api_gen.Active, resp.JSON200.LocationMonitor.Status)
	s.Require().Equal(api_gen.Unknown, resp.JSON200.LocationMonitor.CurrentWeatherDescription)
	s.Require().Equal(expectedLocation.CurrentMaxTemp, resp.JSON200.LocationMonitor.CurrentMaxTemp)
	s.Require().Equal(expectedLocation.CurrentMinTemp, resp.JSON200.LocationMonitor.CurrentMinTemp)
}

func (s *APITestSuite) TestGetLocationMonitorNotFound() {
	locationUUID := uuid.New()

	s.store.EXPECT().GetLocationMonitor(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, id uuid.UUID) (*store.LocationMonitor, error) {
			return nil, store.ErrRecordNotFound
		})

	httpResp, err := s.get(fmt.Sprintf("/locations/%s", locationUUID), nil)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusNotFound, httpResp.StatusCode)

	resp, err := api_gen.ParseGetLocationMonitorResponse(httpResp)
	s.Require().NoError(err)
	s.Require().NotNil(resp.JSON404)
}

func (s *APITestSuite) TestGetLocationMonitorBadUUID() {
	httpResp, err := s.get("/locations/abcd", nil)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusBadRequest, httpResp.StatusCode)
}

func (s *APITestSuite) TestListLocationMonitors() {
	expectedLocation1 := &store.LocationMonitor{
		UUID:       uuid.New(),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Name:       "Virginia Beach, Virginia",
		DataSource: data_source_nws.DataSourceNameNWS_v1,
		Longitude:  -75.97799,
		Latitude:   36.85293,
		Status:     store.LocationMonitorStatusActive,
	}
	expectedLocation2 := &store.LocationMonitor{
		UUID:       uuid.New(),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Name:       "Virginia Beach, Virginia",
		DataSource: data_source_nws.DataSourceNameNWS_v1,
		Longitude:  -75.97799,
		Latitude:   36.85293,
		Status:     store.LocationMonitorStatusActive,
	}
	expectedPageToken := "2023-09-25T20:49:37.256796-04:00;1245"
	s.store.EXPECT().ListLocationMonitors(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, opts store.ListLocationMonitorsOptions) (*store.ListLocationMonitorsPage, error) {
			s.Require().NotNil(opts.PaginationToken)
			s.Require().Equal(expectedPageToken, *opts.PaginationToken)
			return &store.ListLocationMonitorsPage{
				LocationMonitors: []*store.LocationMonitor{expectedLocation1, expectedLocation2},
			}, nil
		})

	httpResp, err := s.get("/locations", map[string]string{"page-token": expectedPageToken})
	s.Require().NoError(err)

	s.Require().Equal(http.StatusOK, httpResp.StatusCode)

	resp, err := api_gen.ParseListLocationMonitorsResponse(httpResp)
	s.Require().NoError(err)
	s.Require().NotNil(resp.JSON200)
	s.Require().Len(resp.JSON200.LocationMonitors, 2)
	s.Require().Nil(resp.JSON200.NextPageToken)

	requireEqual := func(expected *store.LocationMonitor, actual api_gen.LocationMonitor) {
		s.Require().Equal(expected.UUID.String(), actual.Uuid)
		s.Require().Equal(expected.Name, actual.Name)
		s.Require().Equal(api_gen.LocationMonitorDataSource(expected.DataSource), actual.DataSource)
		s.Require().Equal(expected.Longitude, actual.Longitude)
		s.Require().Equal(expected.Latitude, actual.Latitude)
		s.Require().Equal(api_gen.LocationMonitorStatus(expected.Status), actual.Status)
	}

	requireEqual(expectedLocation1, resp.JSON200.LocationMonitors[0])
	requireEqual(expectedLocation2, resp.JSON200.LocationMonitors[1])
}

func (s *APITestSuite) TestRecordWeatherObservation() {
	locationUUID := uuid.New()

	s.store.EXPECT().GetLocationMonitor(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, id uuid.UUID) (*store.LocationMonitor, error) {
			s.Require().Equal(locationUUID.String(), id.String())
			return nil, nil
		})

	req := api_gen.RecordWeatherObservationJSONRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.Clear,
			CurrentMinTemp:            64.0,
			CurrentMaxTemp:            74.0,
		},
	}

	s.store.EXPECT().RecordWeatherObservation(gomock.Any(), locationUUID, gomock.Any()).DoAndReturn(
		func(ctx context.Context, locationID uuid.UUID, observation data_source_base.WeatherObservation) error {
			s.Require().Equal(
				data_source_base.WeatherDescription(req.Observation.CurrentWeatherDescription),
				observation.CurrentDescription)
			s.Require().Equal(req.Observation.CurrentMinTemp, observation.CurrentMinTemp)
			s.Require().Equal(req.Observation.CurrentMaxTemp, observation.CurrentMaxTemp)
			return nil
		},
	)

	httpResp, err := s.postObject(fmt.Sprintf("/locations/%s/observations", locationUUID.String()), req)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusOK, httpResp.StatusCode)
}

func (s *APITestSuite) TestRecordWeatherObservationInvalidDescription() {
	locationUUID := uuid.New()

	req := api_gen.RecordWeatherObservationJSONRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.WeatherDescription("bad"),
			CurrentMinTemp:            64.0,
			CurrentMaxTemp:            74.0,
		},
	}

	httpResp, err := s.postObject(fmt.Sprintf("/locations/%s/observations", locationUUID.String()), req)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusBadRequest, httpResp.StatusCode)
}

func (s *APITestSuite) TestRecordWeatherObservationBadUUID() {
	req := api_gen.RecordWeatherObservationJSONRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.Clear,
			CurrentMinTemp:            64.0,
			CurrentMaxTemp:            74.0,
		},
	}

	httpResp, err := s.postObject("/locations/bad/observations", req)
	s.Require().NoError(err)

	s.Require().Equal(http.StatusBadRequest, httpResp.StatusCode)
}

func (s *APITestSuite) TestDeletetLocationMonitor() {
	expectedUUID := uuid.New()

	s.store.EXPECT().DeleteLocationMonitor(gomock.Any(), gomock.Any()).DoAndReturn(
		func(ctx context.Context, id uuid.UUID) error {
			s.Require().Equal(expectedUUID.String(), id.String())
			return nil
		})

	httpResp, err := s.delete(fmt.Sprintf("/locations/%s", expectedUUID.String()))
	s.Require().NoError(err)

	s.Require().Equal(http.StatusOK, httpResp.StatusCode)
}

func TestAPITestSuite(t *testing.T) {
	suite.Run(t, new(APITestSuite))
}
