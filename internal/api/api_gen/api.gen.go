// Package api_gen provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen version v1.15.0 DO NOT EDIT.
package api_gen

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"
	"github.com/oapi-codegen/runtime"
)

// Defines values for LocationMonitorDataSource.
const (
	LocationMonitorDataSourceMockv1 LocationMonitorDataSource = "mock/v1"
	LocationMonitorDataSourceNoopv1 LocationMonitorDataSource = "noop/v1"
	LocationMonitorDataSourceNwsv1  LocationMonitorDataSource = "nws/v1"
)

// Defines values for LocationMonitorStatus.
const (
	Active LocationMonitorStatus = "active"
)

// Defines values for NewLocationMonitorDataSource.
const (
	NewLocationMonitorDataSourceMockv1 NewLocationMonitorDataSource = "mock/v1"
	NewLocationMonitorDataSourceNoopv1 NewLocationMonitorDataSource = "noop/v1"
	NewLocationMonitorDataSourceNwsv1  NewLocationMonitorDataSource = "nws/v1"
)

// Defines values for WeatherDescription.
const (
	Clear        WeatherDescription = "clear"
	Clouds       WeatherDescription = "clouds"
	Extreme      WeatherDescription = "extreme"
	Fog          WeatherDescription = "fog"
	FreezingRain WeatherDescription = "freezing_rain"
	Rain         WeatherDescription = "rain"
	Snow         WeatherDescription = "snow"
	Thunderstorm WeatherDescription = "thunderstorm"
	Unknown      WeatherDescription = "unknown"
)

// CreateLocationMonitorRequestBody defines model for CreateLocationMonitorRequestBody.
type CreateLocationMonitorRequestBody struct {
	LocationMonitor NewLocationMonitor `json:"location_monitor"`
}

// Error defines model for Error.
type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// ErrorResponse defines model for ErrorResponse.
type ErrorResponse struct {
	Error Error `json:"error"`
}

// GetLocationMonitorResponseBody defines model for GetLocationMonitorResponseBody.
type GetLocationMonitorResponseBody struct {
	LocationMonitor LocationMonitor `json:"location_monitor"`
}

// ListLocationMonitorsResponseBody defines model for ListLocationMonitorsResponseBody.
type ListLocationMonitorsResponseBody struct {
	LocationMonitors []LocationMonitor `json:"location_monitors"`
	NextPageToken    *string           `json:"next_page_token,omitempty"`
}

// LocationMonitor defines model for LocationMonitor.
type LocationMonitor struct {
	CreatedAt                 string                    `json:"created_at"`
	CurrentMaxTemp            *float64                  `json:"current_max_temp,omitempty"`
	CurrentMinTemp            *float64                  `json:"current_min_temp,omitempty"`
	CurrentWeatherDescription WeatherDescription        `json:"current_weather_description"`
	DataSource                LocationMonitorDataSource `json:"data_source"`
	Latitude                  float64                   `json:"latitude"`
	Longitude                 float64                   `json:"longitude"`
	Name                      string                    `json:"name"`
	Status                    LocationMonitorStatus     `json:"status"`
	UpdatedAt                 string                    `json:"updated_at"`
	Uuid                      string                    `json:"uuid"`
}

// LocationMonitorDataSource defines model for LocationMonitor.DataSource.
type LocationMonitorDataSource string

// LocationMonitorStatus defines model for LocationMonitor.Status.
type LocationMonitorStatus string

// NewLocationMonitor defines model for NewLocationMonitor.
type NewLocationMonitor struct {
	DataSource NewLocationMonitorDataSource `json:"data_source"`
	Latitude   float64                      `json:"latitude"`
	Longitude  float64                      `json:"longitude"`
	Name       *string                      `json:"name,omitempty"`
}

// NewLocationMonitorDataSource defines model for NewLocationMonitor.DataSource.
type NewLocationMonitorDataSource string

// RecordWeatherObservationRequestBody defines model for RecordWeatherObservationRequestBody.
type RecordWeatherObservationRequestBody struct {
	Observation WeatherObservation `json:"observation"`
}

// WeatherDescription defines model for WeatherDescription.
type WeatherDescription string

// WeatherObservation defines model for WeatherObservation.
type WeatherObservation struct {
	CurrentMaxTemp            float64            `json:"current_max_temp"`
	CurrentMinTemp            float64            `json:"current_min_temp"`
	CurrentWeatherDescription WeatherDescription `json:"current_weather_description"`
}

// ListLocationMonitorsParams defines parameters for ListLocationMonitors.
type ListLocationMonitorsParams struct {
	// PageToken The token identifier for a given page.
	PageToken *string `form:"page-token,omitempty" json:"page-token,omitempty"`
}

// CreateLocationMonitorJSONRequestBody defines body for CreateLocationMonitor for application/json ContentType.
type CreateLocationMonitorJSONRequestBody = CreateLocationMonitorRequestBody

// RecordWeatherObservationJSONRequestBody defines body for RecordWeatherObservation for application/json ContentType.
type RecordWeatherObservationJSONRequestBody = RecordWeatherObservationRequestBody

// RequestEditorFn  is the function signature for the RequestEditor callback function
type RequestEditorFn func(ctx context.Context, req *http.Request) error

// Doer performs HTTP requests.
//
// The standard http.Client implements this interface.
type HttpRequestDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client which conforms to the OpenAPI3 specification for this service.
type Client struct {
	// The endpoint of the server conforming to this interface, with scheme,
	// https://api.deepmap.com for example. This can contain a path relative
	// to the server, such as https://api.deepmap.com/dev-test, and all the
	// paths in the swagger spec will be appended to the server.
	Server string

	// Doer for performing requests, typically a *http.Client with any
	// customized settings, such as certificate chains.
	Client HttpRequestDoer

	// A list of callbacks for modifying requests which are generated before sending over
	// the network.
	RequestEditors []RequestEditorFn
}

// ClientOption allows setting custom parameters during construction
type ClientOption func(*Client) error

// Creates a new Client, with reasonable defaults
func NewClient(server string, opts ...ClientOption) (*Client, error) {
	// create a client with sane default values
	client := Client{
		Server: server,
	}
	// mutate client and add all optional params
	for _, o := range opts {
		if err := o(&client); err != nil {
			return nil, err
		}
	}
	// ensure the server URL always has a trailing slash
	if !strings.HasSuffix(client.Server, "/") {
		client.Server += "/"
	}
	// create httpClient, if not already present
	if client.Client == nil {
		client.Client = &http.Client{}
	}
	return &client, nil
}

// WithHTTPClient allows overriding the default Doer, which is
// automatically created using http.Client. This is useful for tests.
func WithHTTPClient(doer HttpRequestDoer) ClientOption {
	return func(c *Client) error {
		c.Client = doer
		return nil
	}
}

// WithRequestEditorFn allows setting up a callback function, which will be
// called right before sending the request. This can be used to mutate the request.
func WithRequestEditorFn(fn RequestEditorFn) ClientOption {
	return func(c *Client) error {
		c.RequestEditors = append(c.RequestEditors, fn)
		return nil
	}
}

// The interface specification for the client above.
type ClientInterface interface {
	// ListLocationMonitors request
	ListLocationMonitors(ctx context.Context, params *ListLocationMonitorsParams, reqEditors ...RequestEditorFn) (*http.Response, error)

	// CreateLocationMonitorWithBody request with any body
	CreateLocationMonitorWithBody(ctx context.Context, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error)

	CreateLocationMonitor(ctx context.Context, body CreateLocationMonitorJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error)

	// DeleteLocationMonitor request
	DeleteLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error)

	// GetLocationMonitor request
	GetLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error)

	// RecordWeatherObservationWithBody request with any body
	RecordWeatherObservationWithBody(ctx context.Context, locationId string, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error)

	RecordWeatherObservation(ctx context.Context, locationId string, body RecordWeatherObservationJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error)
}

func (c *Client) ListLocationMonitors(ctx context.Context, params *ListLocationMonitorsParams, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewListLocationMonitorsRequest(c.Server, params)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) CreateLocationMonitorWithBody(ctx context.Context, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewCreateLocationMonitorRequestWithBody(c.Server, contentType, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) CreateLocationMonitor(ctx context.Context, body CreateLocationMonitorJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewCreateLocationMonitorRequest(c.Server, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) DeleteLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewDeleteLocationMonitorRequest(c.Server, locationId)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GetLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGetLocationMonitorRequest(c.Server, locationId)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) RecordWeatherObservationWithBody(ctx context.Context, locationId string, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewRecordWeatherObservationRequestWithBody(c.Server, locationId, contentType, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) RecordWeatherObservation(ctx context.Context, locationId string, body RecordWeatherObservationJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewRecordWeatherObservationRequest(c.Server, locationId, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

// NewListLocationMonitorsRequest generates requests for ListLocationMonitors
func NewListLocationMonitorsRequest(server string, params *ListLocationMonitorsParams) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/locations")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	if params != nil {
		queryValues := queryURL.Query()

		if params.PageToken != nil {

			if queryFrag, err := runtime.StyleParamWithLocation("form", true, "page-token", runtime.ParamLocationQuery, *params.PageToken); err != nil {
				return nil, err
			} else if parsed, err := url.ParseQuery(queryFrag); err != nil {
				return nil, err
			} else {
				for k, v := range parsed {
					for _, v2 := range v {
						queryValues.Add(k, v2)
					}
				}
			}

		}

		queryURL.RawQuery = queryValues.Encode()
	}

	req, err := http.NewRequest("GET", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

// NewCreateLocationMonitorRequest calls the generic CreateLocationMonitor builder with application/json body
func NewCreateLocationMonitorRequest(server string, body CreateLocationMonitorJSONRequestBody) (*http.Request, error) {
	var bodyReader io.Reader
	buf, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	bodyReader = bytes.NewReader(buf)
	return NewCreateLocationMonitorRequestWithBody(server, "application/json", bodyReader)
}

// NewCreateLocationMonitorRequestWithBody generates requests for CreateLocationMonitor with any type of body
func NewCreateLocationMonitorRequestWithBody(server string, contentType string, body io.Reader) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/locations")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", queryURL.String(), body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", contentType)

	return req, nil
}

// NewDeleteLocationMonitorRequest generates requests for DeleteLocationMonitor
func NewDeleteLocationMonitorRequest(server string, locationId string) (*http.Request, error) {
	var err error

	var pathParam0 string

	pathParam0, err = runtime.StyleParamWithLocation("simple", false, "location-id", runtime.ParamLocationPath, locationId)
	if err != nil {
		return nil, err
	}

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/locations/%s", pathParam0)
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("DELETE", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

// NewGetLocationMonitorRequest generates requests for GetLocationMonitor
func NewGetLocationMonitorRequest(server string, locationId string) (*http.Request, error) {
	var err error

	var pathParam0 string

	pathParam0, err = runtime.StyleParamWithLocation("simple", false, "location-id", runtime.ParamLocationPath, locationId)
	if err != nil {
		return nil, err
	}

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/locations/%s", pathParam0)
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

// NewRecordWeatherObservationRequest calls the generic RecordWeatherObservation builder with application/json body
func NewRecordWeatherObservationRequest(server string, locationId string, body RecordWeatherObservationJSONRequestBody) (*http.Request, error) {
	var bodyReader io.Reader
	buf, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	bodyReader = bytes.NewReader(buf)
	return NewRecordWeatherObservationRequestWithBody(server, locationId, "application/json", bodyReader)
}

// NewRecordWeatherObservationRequestWithBody generates requests for RecordWeatherObservation with any type of body
func NewRecordWeatherObservationRequestWithBody(server string, locationId string, contentType string, body io.Reader) (*http.Request, error) {
	var err error

	var pathParam0 string

	pathParam0, err = runtime.StyleParamWithLocation("simple", false, "location-id", runtime.ParamLocationPath, locationId)
	if err != nil {
		return nil, err
	}

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/locations/%s/observations", pathParam0)
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", queryURL.String(), body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", contentType)

	return req, nil
}

func (c *Client) applyEditors(ctx context.Context, req *http.Request, additionalEditors []RequestEditorFn) error {
	for _, r := range c.RequestEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	for _, r := range additionalEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	return nil
}

// ClientWithResponses builds on ClientInterface to offer response payloads
type ClientWithResponses struct {
	ClientInterface
}

// NewClientWithResponses creates a new ClientWithResponses, which wraps
// Client with return type handling
func NewClientWithResponses(server string, opts ...ClientOption) (*ClientWithResponses, error) {
	client, err := NewClient(server, opts...)
	if err != nil {
		return nil, err
	}
	return &ClientWithResponses{client}, nil
}

// WithBaseURL overrides the baseURL.
func WithBaseURL(baseURL string) ClientOption {
	return func(c *Client) error {
		newBaseURL, err := url.Parse(baseURL)
		if err != nil {
			return err
		}
		c.Server = newBaseURL.String()
		return nil
	}
}

// ClientWithResponsesInterface is the interface specification for the client with responses above.
type ClientWithResponsesInterface interface {
	// ListLocationMonitorsWithResponse request
	ListLocationMonitorsWithResponse(ctx context.Context, params *ListLocationMonitorsParams, reqEditors ...RequestEditorFn) (*ListLocationMonitorsResponse, error)

	// CreateLocationMonitorWithBodyWithResponse request with any body
	CreateLocationMonitorWithBodyWithResponse(ctx context.Context, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*CreateLocationMonitorResponse, error)

	CreateLocationMonitorWithResponse(ctx context.Context, body CreateLocationMonitorJSONRequestBody, reqEditors ...RequestEditorFn) (*CreateLocationMonitorResponse, error)

	// DeleteLocationMonitorWithResponse request
	DeleteLocationMonitorWithResponse(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*DeleteLocationMonitorResponse, error)

	// GetLocationMonitorWithResponse request
	GetLocationMonitorWithResponse(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*GetLocationMonitorResponse, error)

	// RecordWeatherObservationWithBodyWithResponse request with any body
	RecordWeatherObservationWithBodyWithResponse(ctx context.Context, locationId string, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*RecordWeatherObservationResponse, error)

	RecordWeatherObservationWithResponse(ctx context.Context, locationId string, body RecordWeatherObservationJSONRequestBody, reqEditors ...RequestEditorFn) (*RecordWeatherObservationResponse, error)
}

type ListLocationMonitorsResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *ListLocationMonitorsResponseBody
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r ListLocationMonitorsResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r ListLocationMonitorsResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type CreateLocationMonitorResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *GetLocationMonitorResponseBody
	JSON400      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r CreateLocationMonitorResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r CreateLocationMonitorResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type DeleteLocationMonitorResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON400      *ErrorResponse
	JSON404      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r DeleteLocationMonitorResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r DeleteLocationMonitorResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type GetLocationMonitorResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *GetLocationMonitorResponseBody
	JSON400      *ErrorResponse
	JSON404      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r GetLocationMonitorResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GetLocationMonitorResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type RecordWeatherObservationResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON400      *ErrorResponse
	JSON404      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r RecordWeatherObservationResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r RecordWeatherObservationResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

// ListLocationMonitorsWithResponse request returning *ListLocationMonitorsResponse
func (c *ClientWithResponses) ListLocationMonitorsWithResponse(ctx context.Context, params *ListLocationMonitorsParams, reqEditors ...RequestEditorFn) (*ListLocationMonitorsResponse, error) {
	rsp, err := c.ListLocationMonitors(ctx, params, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseListLocationMonitorsResponse(rsp)
}

// CreateLocationMonitorWithBodyWithResponse request with arbitrary body returning *CreateLocationMonitorResponse
func (c *ClientWithResponses) CreateLocationMonitorWithBodyWithResponse(ctx context.Context, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*CreateLocationMonitorResponse, error) {
	rsp, err := c.CreateLocationMonitorWithBody(ctx, contentType, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseCreateLocationMonitorResponse(rsp)
}

func (c *ClientWithResponses) CreateLocationMonitorWithResponse(ctx context.Context, body CreateLocationMonitorJSONRequestBody, reqEditors ...RequestEditorFn) (*CreateLocationMonitorResponse, error) {
	rsp, err := c.CreateLocationMonitor(ctx, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseCreateLocationMonitorResponse(rsp)
}

// DeleteLocationMonitorWithResponse request returning *DeleteLocationMonitorResponse
func (c *ClientWithResponses) DeleteLocationMonitorWithResponse(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*DeleteLocationMonitorResponse, error) {
	rsp, err := c.DeleteLocationMonitor(ctx, locationId, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseDeleteLocationMonitorResponse(rsp)
}

// GetLocationMonitorWithResponse request returning *GetLocationMonitorResponse
func (c *ClientWithResponses) GetLocationMonitorWithResponse(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*GetLocationMonitorResponse, error) {
	rsp, err := c.GetLocationMonitor(ctx, locationId, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGetLocationMonitorResponse(rsp)
}

// RecordWeatherObservationWithBodyWithResponse request with arbitrary body returning *RecordWeatherObservationResponse
func (c *ClientWithResponses) RecordWeatherObservationWithBodyWithResponse(ctx context.Context, locationId string, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*RecordWeatherObservationResponse, error) {
	rsp, err := c.RecordWeatherObservationWithBody(ctx, locationId, contentType, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseRecordWeatherObservationResponse(rsp)
}

func (c *ClientWithResponses) RecordWeatherObservationWithResponse(ctx context.Context, locationId string, body RecordWeatherObservationJSONRequestBody, reqEditors ...RequestEditorFn) (*RecordWeatherObservationResponse, error) {
	rsp, err := c.RecordWeatherObservation(ctx, locationId, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseRecordWeatherObservationResponse(rsp)
}

// ParseListLocationMonitorsResponse parses an HTTP response from a ListLocationMonitorsWithResponse call
func ParseListLocationMonitorsResponse(rsp *http.Response) (*ListLocationMonitorsResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &ListLocationMonitorsResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest ListLocationMonitorsResponseBody
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseCreateLocationMonitorResponse parses an HTTP response from a CreateLocationMonitorWithResponse call
func ParseCreateLocationMonitorResponse(rsp *http.Response) (*CreateLocationMonitorResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &CreateLocationMonitorResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest GetLocationMonitorResponseBody
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseDeleteLocationMonitorResponse parses an HTTP response from a DeleteLocationMonitorWithResponse call
func ParseDeleteLocationMonitorResponse(rsp *http.Response) (*DeleteLocationMonitorResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &DeleteLocationMonitorResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 404:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON404 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseGetLocationMonitorResponse parses an HTTP response from a GetLocationMonitorWithResponse call
func ParseGetLocationMonitorResponse(rsp *http.Response) (*GetLocationMonitorResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GetLocationMonitorResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest GetLocationMonitorResponseBody
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 404:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON404 = &dest

	}

	return response, nil
}

// ParseRecordWeatherObservationResponse parses an HTTP response from a RecordWeatherObservationWithResponse call
func ParseRecordWeatherObservationResponse(rsp *http.Response) (*RecordWeatherObservationResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &RecordWeatherObservationResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 404:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON404 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ServerInterface represents all server handlers.
type ServerInterface interface {

	// (GET /locations)
	ListLocationMonitors(w http.ResponseWriter, r *http.Request, params ListLocationMonitorsParams)

	// (POST /locations)
	CreateLocationMonitor(w http.ResponseWriter, r *http.Request)

	// (DELETE /locations/{location-id})
	DeleteLocationMonitor(w http.ResponseWriter, r *http.Request, locationId string)

	// (GET /locations/{location-id})
	GetLocationMonitor(w http.ResponseWriter, r *http.Request, locationId string)

	// (POST /locations/{location-id}/observations)
	RecordWeatherObservation(w http.ResponseWriter, r *http.Request, locationId string)
}

// ServerInterfaceWrapper converts contexts to parameters.
type ServerInterfaceWrapper struct {
	Handler            ServerInterface
	HandlerMiddlewares []MiddlewareFunc
	ErrorHandlerFunc   func(w http.ResponseWriter, r *http.Request, err error)
}

type MiddlewareFunc func(http.Handler) http.Handler

// ListLocationMonitors operation middleware
func (siw *ServerInterfaceWrapper) ListLocationMonitors(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var err error

	// Parameter object where we will unmarshal all parameters from the context
	var params ListLocationMonitorsParams

	// ------------- Optional query parameter "page-token" -------------

	err = runtime.BindQueryParameter("form", true, false, "page-token", r.URL.Query(), &params.PageToken)
	if err != nil {
		siw.ErrorHandlerFunc(w, r, &InvalidParamFormatError{ParamName: "page-token", Err: err})
		return
	}

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.ListLocationMonitors(w, r, params)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

// CreateLocationMonitor operation middleware
func (siw *ServerInterfaceWrapper) CreateLocationMonitor(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.CreateLocationMonitor(w, r)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

// DeleteLocationMonitor operation middleware
func (siw *ServerInterfaceWrapper) DeleteLocationMonitor(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var err error

	// ------------- Path parameter "location-id" -------------
	var locationId string

	err = runtime.BindStyledParameter("simple", false, "location-id", mux.Vars(r)["location-id"], &locationId)
	if err != nil {
		siw.ErrorHandlerFunc(w, r, &InvalidParamFormatError{ParamName: "location-id", Err: err})
		return
	}

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.DeleteLocationMonitor(w, r, locationId)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

// GetLocationMonitor operation middleware
func (siw *ServerInterfaceWrapper) GetLocationMonitor(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var err error

	// ------------- Path parameter "location-id" -------------
	var locationId string

	err = runtime.BindStyledParameter("simple", false, "location-id", mux.Vars(r)["location-id"], &locationId)
	if err != nil {
		siw.ErrorHandlerFunc(w, r, &InvalidParamFormatError{ParamName: "location-id", Err: err})
		return
	}

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.GetLocationMonitor(w, r, locationId)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

// RecordWeatherObservation operation middleware
func (siw *ServerInterfaceWrapper) RecordWeatherObservation(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var err error

	// ------------- Path parameter "location-id" -------------
	var locationId string

	err = runtime.BindStyledParameter("simple", false, "location-id", mux.Vars(r)["location-id"], &locationId)
	if err != nil {
		siw.ErrorHandlerFunc(w, r, &InvalidParamFormatError{ParamName: "location-id", Err: err})
		return
	}

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.RecordWeatherObservation(w, r, locationId)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

type UnescapedCookieParamError struct {
	ParamName string
	Err       error
}

func (e *UnescapedCookieParamError) Error() string {
	return fmt.Sprintf("error unescaping cookie parameter '%s'", e.ParamName)
}

func (e *UnescapedCookieParamError) Unwrap() error {
	return e.Err
}

type UnmarshalingParamError struct {
	ParamName string
	Err       error
}

func (e *UnmarshalingParamError) Error() string {
	return fmt.Sprintf("Error unmarshaling parameter %s as JSON: %s", e.ParamName, e.Err.Error())
}

func (e *UnmarshalingParamError) Unwrap() error {
	return e.Err
}

type RequiredParamError struct {
	ParamName string
}

func (e *RequiredParamError) Error() string {
	return fmt.Sprintf("Query argument %s is required, but not found", e.ParamName)
}

type RequiredHeaderError struct {
	ParamName string
	Err       error
}

func (e *RequiredHeaderError) Error() string {
	return fmt.Sprintf("Header parameter %s is required, but not found", e.ParamName)
}

func (e *RequiredHeaderError) Unwrap() error {
	return e.Err
}

type InvalidParamFormatError struct {
	ParamName string
	Err       error
}

func (e *InvalidParamFormatError) Error() string {
	return fmt.Sprintf("Invalid format for parameter %s: %s", e.ParamName, e.Err.Error())
}

func (e *InvalidParamFormatError) Unwrap() error {
	return e.Err
}

type TooManyValuesForParamError struct {
	ParamName string
	Count     int
}

func (e *TooManyValuesForParamError) Error() string {
	return fmt.Sprintf("Expected one value for %s, got %d", e.ParamName, e.Count)
}

// Handler creates http.Handler with routing matching OpenAPI spec.
func Handler(si ServerInterface) http.Handler {
	return HandlerWithOptions(si, GorillaServerOptions{})
}

type GorillaServerOptions struct {
	BaseURL          string
	BaseRouter       *mux.Router
	Middlewares      []MiddlewareFunc
	ErrorHandlerFunc func(w http.ResponseWriter, r *http.Request, err error)
}

// HandlerFromMux creates http.Handler with routing matching OpenAPI spec based on the provided mux.
func HandlerFromMux(si ServerInterface, r *mux.Router) http.Handler {
	return HandlerWithOptions(si, GorillaServerOptions{
		BaseRouter: r,
	})
}

func HandlerFromMuxWithBaseURL(si ServerInterface, r *mux.Router, baseURL string) http.Handler {
	return HandlerWithOptions(si, GorillaServerOptions{
		BaseURL:    baseURL,
		BaseRouter: r,
	})
}

// HandlerWithOptions creates http.Handler with additional options
func HandlerWithOptions(si ServerInterface, options GorillaServerOptions) http.Handler {
	r := options.BaseRouter

	if r == nil {
		r = mux.NewRouter()
	}
	if options.ErrorHandlerFunc == nil {
		options.ErrorHandlerFunc = func(w http.ResponseWriter, r *http.Request, err error) {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}
	wrapper := ServerInterfaceWrapper{
		Handler:            si,
		HandlerMiddlewares: options.Middlewares,
		ErrorHandlerFunc:   options.ErrorHandlerFunc,
	}

	r.HandleFunc(options.BaseURL+"/locations", wrapper.ListLocationMonitors).Methods("GET")

	r.HandleFunc(options.BaseURL+"/locations", wrapper.CreateLocationMonitor).Methods("POST")

	r.HandleFunc(options.BaseURL+"/locations/{location-id}", wrapper.DeleteLocationMonitor).Methods("DELETE")

	r.HandleFunc(options.BaseURL+"/locations/{location-id}", wrapper.GetLocationMonitor).Methods("GET")

	r.HandleFunc(options.BaseURL+"/locations/{location-id}/observations", wrapper.RecordWeatherObservation).Methods("POST")

	return r
}
