package api_gen

import (
	"context"
	"io"
	"net/http"

	"go.opentelemetry.io/otel/trace"
)

type TracedClient struct {
	tracer trace.Tracer
	client ClientInterface
}

func (c *Client) WithTracer(tp trace.TracerProvider) *TracedClient {
	return &TracedClient{tracer: tp.Tracer("golang_demo_client"), client: c}
}

func (c *TracedClient) ListLocationMonitors(ctx context.Context, params *ListLocationMonitorsParams, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.ListLocationMonitors")
	defer span.End()
	return c.client.ListLocationMonitors(ctx, params, reqEditors...)
}

func (c *TracedClient) CreateLocationMonitorWithBody(ctx context.Context, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.CreateLocationMonitorWithBody")
	defer span.End()
	return c.client.CreateLocationMonitorWithBody(ctx, contentType, body, reqEditors...)
}

func (c *TracedClient) CreateLocationMonitor(ctx context.Context, body CreateLocationMonitorJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.CreateLocationMonitor")
	defer span.End()
	return c.client.CreateLocationMonitor(ctx, body, reqEditors...)
}

func (c *TracedClient) DeleteLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.DeleteLocationMonitor")
	defer span.End()
	return c.client.DeleteLocationMonitor(ctx, locationId, reqEditors...)
}

func (c *TracedClient) GetLocationMonitor(ctx context.Context, locationId string, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.GetLocationMonitor")
	defer span.End()
	return c.client.GetLocationMonitor(ctx, locationId, reqEditors...)
}

func (c *TracedClient) RecordWeatherObservationWithBody(ctx context.Context, locationId string, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.RecordWeatherObservationWithBody")
	defer span.End()
	return c.client.RecordWeatherObservationWithBody(ctx, locationId, contentType, body, reqEditors...)
}

func (c *TracedClient) RecordWeatherObservation(ctx context.Context, locationId string, body RecordWeatherObservationJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error) {
	ctx, span := c.tracer.Start(ctx, "Client.RecordWeatherObservation")
	defer span.End()
	return c.client.RecordWeatherObservation(ctx, locationId, body, reqEditors...)
}
