package location_monitor_controller

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/dharmab/tomb/v2"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/common_trace"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/server"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

var (
	ErrNoConfiguredSource = errors.New("no configured source")
)

type refreshRequest struct {
	ctx context.Context
	lm  *api_gen.LocationMonitor
}

type LocationMonitorControllerConfig struct {
	Tracer                trace.Tracer
	Sources               map[data_source_base.DataSourceName]data_source_base.DataSource
	Client                api_gen.ClientInterface
	RefreshInterval       time.Duration
	DataSourceWorkerCount int
}

type LocationMonitorController struct {
	tracer trace.Tracer

	client  api_gen.ClientInterface
	sources map[data_source_base.DataSourceName]data_source_base.DataSource

	refreshInterval       time.Duration
	dataSourceWorkerCount int
}

var _ server.Controller = (*LocationMonitorController)(nil)

func NewLocationMonitorController(cfg LocationMonitorControllerConfig) *LocationMonitorController {
	return &LocationMonitorController{
		tracer:                cfg.Tracer,
		client:                cfg.Client,
		sources:               cfg.Sources,
		refreshInterval:       cfg.RefreshInterval,
		dataSourceWorkerCount: cfg.DataSourceWorkerCount,
	}
}

// recordWeather retreives and records a weather observation for the provided location
func (c *LocationMonitorController) recordWeather(ctx context.Context, lm *api_gen.LocationMonitor) {
	ctx, span := c.tracer.Start(ctx, "LocationMonitorController.recordWeather")
	defer span.End()

	span.SetAttributes(
		common_trace.LocationMonitorUUID.String(lm.Uuid),
		common_trace.DataSourceName.String(string(lm.DataSource)),
	)

	if lm.DataSource == data_source_base.DataSourceNameNoop {
		// No-op data source, do nothing.
		return
	}

	source, ok := c.sources[data_source_base.DataSourceName(lm.DataSource)]
	if !ok {
		span.RecordError(ErrNoConfiguredSource)
		span.SetStatus(codes.Error, ErrNoConfiguredSource.Error())
		return
	}

	observation, err := source.GetCurrentWeather(ctx, lm.Longitude, lm.Latitude)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	recordReq := api_gen.RecordWeatherObservationJSONRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.WeatherDescription(observation.CurrentDescription),
			CurrentMinTemp:            observation.CurrentMinTemp,
			CurrentMaxTemp:            observation.CurrentMaxTemp,
		},
	}

	recordResp, err := c.client.RecordWeatherObservation(ctx, lm.Uuid, recordReq)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to record weather observation")
		return
	}

	if recordResp.Body != nil {
		recordResp.Body.Close()
	}

	if recordResp.StatusCode != http.StatusOK {
		span.SetStatus(codes.Error, "failed to record weather observation")
		return
	}
}

// refreshLocations periodically lists all LocationMonitors and triggers a weather observation
func (c *LocationMonitorController) refreshLocations(t *tomb.Tomb, reqChan chan refreshRequest) {
	ctx, span := c.tracer.Start(t.Context(context.Background()), "LocationMonitorController.refreshLocations")
	defer span.End()

	var pageToken *string

PAGE_LOOP:
	for {
		done := func() bool {
			ctx, span := c.tracer.Start(ctx, "LocationMonitorController.refreshLocations.pageLoop")
			defer span.End()

			httpResp, err := c.client.ListLocationMonitors(ctx, &api_gen.ListLocationMonitorsParams{PageToken: pageToken})
			if err != nil {
				span.RecordError(err)
				span.SetStatus(codes.Error, err.Error())
				return true
			}

			resp, err := api_gen.ParseListLocationMonitorsResponse(httpResp)
			if err != nil {
				span.RecordError(err)
				span.SetStatus(codes.Error, err.Error())
				return true
			}

			if resp.StatusCode() != http.StatusOK {
				span.SetStatus(codes.Error, "unexpected status code from API")
				return true
			}

			span.SetAttributes(attribute.Int("count", len(resp.JSON200.LocationMonitors)))

			for _, lm := range resp.JSON200.LocationMonitors {
				lmCopy := lm
				select {
				case reqChan <- refreshRequest{ctx: ctx, lm: &lmCopy}:
					continue
				case <-t.Dying():
					// If controller is shutting down, just end.
					return true
				}
			}

			if resp.JSON200.NextPageToken == nil {
				return true
			}

			pageToken = resp.JSON200.NextPageToken
			return false
		}()
		if done {
			break PAGE_LOOP
		}
	}
}

func (c *LocationMonitorController) dataSourceWorker(t *tomb.Tomb, reqChan chan refreshRequest) {
	for {
		select {
		case <-t.Dying():
			return
		case req := <-reqChan:
			c.recordWeather(req.ctx, req.lm)
		}
	}
}

func (c *LocationMonitorController) refreshWorker(t *tomb.Tomb, reqChan chan refreshRequest) {
	ticker := time.NewTicker(c.refreshInterval)
	defer ticker.Stop()
	for {
		select {
		case <-t.Dying():
			return
		case <-ticker.C:
			c.refreshLocations(t, reqChan)
		}
	}
}

func (c *LocationMonitorController) Start(tomb *tomb.Tomb) error {
	reqChan := make(chan refreshRequest, c.dataSourceWorkerCount)
	for i := 0; i < c.dataSourceWorkerCount; i++ {
		tomb.Go(func() error {
			c.dataSourceWorker(tomb, reqChan)
			return nil
		})
	}
	tomb.Go(func() error {
		c.refreshWorker(tomb, reqChan)
		return nil
	})
	return nil
}
