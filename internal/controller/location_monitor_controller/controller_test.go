package location_monitor_controller

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dharmab/tomb/v2"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_mocks"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	base "gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_mock"
	"go.opentelemetry.io/otel"
	"go.uber.org/mock/gomock"
)

type LocationMonitorControllerTestSuite struct {
	suite.Suite

	tomb       *tomb.Tomb
	mockClient *api_mocks.MockClientInterface
	mockSource *data_source_mock.MockDataSource

	controller *LocationMonitorController
}

func (s *LocationMonitorControllerTestSuite) SetupTest() {
	s.tomb, _ = tomb.WithContext(context.Background())

	mockController := gomock.NewController(s.T())
	s.mockClient = api_mocks.NewMockClientInterface(mockController)
	s.mockSource = data_source_mock.NewMockDataSource(mockController)

	sources := map[base.DataSourceName]base.DataSource{
		data_source_mock.DataSourceNameMock_v1: s.mockSource,
	}

	s.controller = &LocationMonitorController{
		tracer:  otel.GetTracerProvider().Tracer("LocationMonitorController"),
		client:  s.mockClient,
		sources: sources,
	}
}

func (s *LocationMonitorControllerTestSuite) TestRecordWeather() {
	name := "Virginia Beach, Virginia"
	lm := &api_gen.LocationMonitor{
		Uuid:       uuid.New().String(),
		Name:       name,
		DataSource: api_gen.LocationMonitorDataSource(data_source_mock.DataSourceNameMock_v1),
		Longitude:  -75.97799,
		Latitude:   36.85293,
	}

	observation := base.WeatherObservation{
		CurrentDescription: base.WeatherClear,
		CurrentMinTemp:     63.0,
		CurrentMaxTemp:     74.0,
	}

	s.mockSource.EXPECT().
		GetCurrentWeather(gomock.Any(), lm.Longitude, lm.Latitude).
		DoAndReturn(
			func(ctx context.Context, longitude float64, latitude float64) (*base.WeatherObservation, error) {
				return &observation, nil
			},
		)

	s.mockClient.EXPECT().
		RecordWeatherObservation(gomock.Any(), lm.Uuid, gomock.Any()).
		DoAndReturn(
			func(ctx context.Context, locationId string, body api_gen.RecordWeatherObservationJSONRequestBody, reqEditors ...api_gen.RequestEditorFn) (*http.Response, error) {
				s.Require().Equal(api_gen.WeatherDescription(observation.CurrentDescription), body.Observation.CurrentWeatherDescription)
				s.Require().Equal(observation.CurrentMinTemp, body.Observation.CurrentMinTemp)
				s.Require().Equal(observation.CurrentMaxTemp, body.Observation.CurrentMaxTemp)
				recorder := httptest.NewRecorder()
				recorder.WriteHeader(http.StatusOK)
				return recorder.Result(), nil
			},
		)

	s.controller.recordWeather(context.Background(), lm)
}

func (s *LocationMonitorControllerTestSuite) TestRecordWeatherTest() {
	name := "Virginia Beach, Virginia"
	lm := &api_gen.LocationMonitor{
		Uuid:       uuid.New().String(),
		Name:       name,
		DataSource: api_gen.LocationMonitorDataSource(data_source_base.DataSourceNameNoop),
		Longitude:  -75.97799,
		Latitude:   36.85293,
	}

	s.controller.recordWeather(context.Background(), lm)
}

func (s *LocationMonitorControllerTestSuite) TestRefreshLocations() {
	pageToken := "fake"
	listResps := []api_gen.ListLocationMonitorsResponseBody{
		{
			LocationMonitors: []api_gen.LocationMonitor{
				{Uuid: uuid.New().String()},
				{Uuid: uuid.New().String()},
			},
			NextPageToken: &pageToken,
		},
		{
			LocationMonitors: []api_gen.LocationMonitor{
				{Uuid: uuid.New().String()},
				{Uuid: uuid.New().String()},
			},
		},
	}

	calls := 0
	s.mockClient.EXPECT().ListLocationMonitors(gomock.Any(), gomock.Any()).Times(2).DoAndReturn(
		func(ctx context.Context, params *api_gen.ListLocationMonitorsParams, reqEditors ...api_gen.RequestEditorFn) (*http.Response, error) {
			recorder := httptest.NewRecorder()
			recorder.HeaderMap.Set("Content-Type", "application/json")
			recorder.WriteHeader(http.StatusOK)
			err := json.NewEncoder(recorder).Encode(listResps[calls])
			calls += 1
			return recorder.Result(), err
		},
	)

	t := &tomb.Tomb{}
	reqChan := make(chan refreshRequest, 100)

	s.controller.refreshLocations(t, reqChan)

	s.Require().Len(reqChan, 4)
	for i := 0; i < 2; i++ {
		req := <-reqChan
		s.Require().Equal(listResps[0].LocationMonitors[i].Uuid, req.lm.Uuid)
	}
	for i := 0; i < 2; i++ {
		req := <-reqChan
		s.Require().Equal(listResps[1].LocationMonitors[i].Uuid, req.lm.Uuid)
	}
}

func TestLocationMonitorControllerTestSuite(t *testing.T) {
	suite.Run(t, new(LocationMonitorControllerTestSuite))
}
