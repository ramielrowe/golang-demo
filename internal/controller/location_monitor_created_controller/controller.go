package location_monitor_created_controller

import (
	"context"
	"errors"
	"net/http"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/dharmab/tomb/v2"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_events"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/common_trace"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

type Config struct {
	Tracer       trace.Tracer
	Sources      map[data_source_base.DataSourceName]data_source_base.DataSource
	Client       api_gen.ClientInterface
	Subscription *pubsub.Subscription
}

type LocationMonitorCreatedController struct {
	tracer  trace.Tracer
	sub     *pubsub.Subscription
	client  api_gen.ClientInterface
	sources map[data_source_base.DataSourceName]data_source_base.DataSource
}

func NewLocationMonitorCreatedController(cfg Config) *LocationMonitorCreatedController {
	return &LocationMonitorCreatedController{
		tracer:  cfg.Tracer,
		sub:     cfg.Subscription,
		client:  cfg.Client,
		sources: cfg.Sources,
	}
}

func (c *LocationMonitorCreatedController) handleMessage(ctx context.Context, msg *pubsub.Message) {
	ctx = otel.GetTextMapPropagator().Extract(ctx, propagation.MapCarrier(msg.Attributes))
	ctx, span := c.tracer.Start(ctx, "LocationMonitorCreatedController.handleMessage")
	defer span.End()
	common_trace.SetAttrForMsg(span, msg)

	event, err := api_events.UnmarshalEvent[api_events.LocationMonitorCreated](msg.Data)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to unmarshal event")
		common_trace.AckMsg(span, msg)
		return
	}

	if time.Now().Add(-1 * time.Second).After(event.Timestamp) {
		// Give the DB transaction some time to commit
		time.Sleep(1 * time.Second)
	}

	span.SetAttributes(common_trace.LocationMonitorUUID.String(event.Body.UUID.String()))

	httpResp, err := c.client.GetLocationMonitor(ctx, event.Body.UUID.String())
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to get Location Monitor from API")
		common_trace.NackMsg(span, msg)
		return
	}

	resp, err := api_gen.ParseGetLocationMonitorResponse(httpResp)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to parse Get Location Monitor response")
		common_trace.NackMsg(span, msg)
		return
	}

	if httpResp.StatusCode != http.StatusOK {
		if httpResp.StatusCode == http.StatusNotFound {
			if time.Now().Add(-30 * time.Second).After(event.Timestamp) {
				// Give up after 30 seconds
				span.AddEvent("giving up on not found location monitor")
				common_trace.AckMsg(span, msg)
				return
			}
			time.Sleep(5 * time.Second)
		}
		span.RecordError(errors.New("Unexpected status from API: " + httpResp.Status))
		span.SetStatus(codes.Error, "failed to get Location Monitor from API")
		common_trace.NackMsg(span, msg)
		return
	}

	lm := resp.JSON200.LocationMonitor

	span.SetAttributes(common_trace.DataSourceName.String(string(lm.DataSource)))

	if lm.DataSource == data_source_base.DataSourceNameNoop {
		// No-op data source, do nothing.
		common_trace.AckMsg(span, msg)
		return
	}

	source, ok := c.sources[data_source_base.DataSourceName(lm.DataSource)]
	if !ok {
		span.SetStatus(codes.Error, "Couldn't find data source for event")
		common_trace.NackMsg(span, msg)
		return
	}

	observation, err := source.GetCurrentWeather(ctx, lm.Longitude, lm.Latitude)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to get current weather")
		common_trace.NackMsg(span, msg)
		return
	}

	recordReq := api_gen.RecordWeatherObservationJSONRequestBody{
		Observation: api_gen.WeatherObservation{
			CurrentWeatherDescription: api_gen.WeatherDescription(observation.CurrentDescription),
			CurrentMinTemp:            observation.CurrentMinTemp,
			CurrentMaxTemp:            observation.CurrentMaxTemp,
		},
	}

	recordResp, err := c.client.RecordWeatherObservation(ctx, lm.Uuid, recordReq)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "failed to record weather observation")
		return
	}

	if recordResp.Body != nil {
		recordResp.Body.Close()
	}

	if recordResp.StatusCode != http.StatusOK {
		span.SetStatus(codes.Error, "failed to record weather observation")
		common_trace.NackMsg(span, msg)
		return
	}

	common_trace.AckMsg(span, msg)
}

func (c *LocationMonitorCreatedController) Start(t *tomb.Tomb) error {
	bgCtx := t.Context(context.Background())
	t.Go(func() error {
		return c.sub.Receive(
			bgCtx,
			c.handleMessage,
		)
	})
	return nil
}
