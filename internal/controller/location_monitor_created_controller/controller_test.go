package location_monitor_created_controller

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/dharmab/tomb/v2"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_events"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_gen"
	"gitlab.com/ramielrowe/golang-demo/internal/api/api_mocks"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_base"
	"gitlab.com/ramielrowe/golang-demo/internal/data_source/data_source_mock"
	"go.opentelemetry.io/otel"
	"go.uber.org/mock/gomock"
)

type LocationMonitorCreatedControllerTestSuite struct {
	suite.Suite

	tomb       *tomb.Tomb
	mockClient *api_mocks.MockClientInterface
	mockSource *data_source_mock.MockDataSource

	controller *LocationMonitorCreatedController
}

func (s *LocationMonitorCreatedControllerTestSuite) SetupTest() {
	s.tomb, _ = tomb.WithContext(context.Background())

	mockController := gomock.NewController(s.T())
	s.mockClient = api_mocks.NewMockClientInterface(mockController)
	s.mockSource = data_source_mock.NewMockDataSource(mockController)

	sources := map[data_source_base.DataSourceName]data_source_base.DataSource{
		data_source_mock.DataSourceNameMock_v1: s.mockSource,
	}

	s.controller = &LocationMonitorCreatedController{
		tracer:  otel.GetTracerProvider().Tracer("LocationMonitorCreatedController"),
		client:  s.mockClient,
		sources: sources,
	}
}

func (s *LocationMonitorCreatedControllerTestSuite) TestHandleMessage() {
	id := uuid.New()

	event := api_events.Event[api_events.LocationMonitorCreated]{
		Type:      api_events.EventTypeLocationMonitorCreated,
		Timestamp: time.Now().Add(-5 * time.Second),
		Body: api_events.LocationMonitorCreated{
			UUID: id,
		},
	}
	eventData, err := json.Marshal(event)
	s.Require().NoError(err)
	eventMsg := &pubsub.Message{Data: eventData}

	fakeLM := api_gen.GetLocationMonitorResponseBody{
		LocationMonitor: api_gen.LocationMonitor{
			Uuid:       id.String(),
			Name:       "Virginia Beach, Virginia",
			DataSource: api_gen.LocationMonitorDataSource(data_source_mock.DataSourceNameMock_v1),
			Longitude:  -75.97799,
			Latitude:   36.85293,
		},
	}
	s.mockClient.EXPECT().
		GetLocationMonitor(gomock.Any(), id.String()).
		DoAndReturn(
			func(ctx context.Context, locationId string, reqEditors ...api_gen.RequestEditorFn) (*http.Response, error) {
				recorder := httptest.NewRecorder()
				recorder.Header().Add("Content-Type", "application/json")
				recorder.WriteHeader(http.StatusOK)
				s.Require().NoError(json.NewEncoder(recorder).Encode(fakeLM))
				return recorder.Result(), nil
			},
		)

	observation := data_source_base.WeatherObservation{
		CurrentDescription: data_source_base.WeatherClear,
		CurrentMinTemp:     63.0,
		CurrentMaxTemp:     74.0,
	}
	s.mockSource.EXPECT().
		GetCurrentWeather(gomock.Any(), fakeLM.LocationMonitor.Longitude, fakeLM.LocationMonitor.Latitude).
		DoAndReturn(
			func(ctx context.Context, longitude float64, latitude float64) (*data_source_base.WeatherObservation, error) {
				return &observation, nil
			},
		)

	s.mockClient.EXPECT().
		RecordWeatherObservation(gomock.Any(), fakeLM.LocationMonitor.Uuid, gomock.Any()).
		DoAndReturn(
			func(ctx context.Context, locationId string, body api_gen.RecordWeatherObservationJSONRequestBody, reqEditors ...api_gen.RequestEditorFn) (*http.Response, error) {
				s.Require().Equal(api_gen.WeatherDescription(observation.CurrentDescription), body.Observation.CurrentWeatherDescription)
				s.Require().Equal(observation.CurrentMinTemp, body.Observation.CurrentMinTemp)
				s.Require().Equal(observation.CurrentMaxTemp, body.Observation.CurrentMaxTemp)
				recorder := httptest.NewRecorder()
				recorder.WriteHeader(http.StatusOK)
				return recorder.Result(), nil
			},
		)

	s.controller.handleMessage(context.Background(), eventMsg)
}

func TestLocationMonitorCreatedControllerTestSuite(t *testing.T) {
	suite.Run(t, new(LocationMonitorCreatedControllerTestSuite))
}
